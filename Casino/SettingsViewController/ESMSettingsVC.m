//
//  SettingsViewController.m
//  Casino
//
//  Created by Sergey Yefanov on 04.08.14.
//  Copyright (c) 2014 Sergey Yefanov. All rights reserved.
//

#import "ESMSettingsVC.h"
#import "ESMSaveManager.h"
#import "ESMFileManager.h"

@interface ESMSettingsVC ()

@property (nonatomic) UIButton *btnMenu;

@end

@implementation ESMSettingsVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	// button menu
	
	self.btnMenu = [UIButton buttonWithType:UIButtonTypeCustom];
	self.btnMenu.frame = CGRectMake(10, 10, 150, 100);
	[self.btnMenu addTarget:self action:@selector(didTouchMenu) forControlEvents:UIControlEventTouchUpInside];
	[self.btnMenu setImage:[UIImage imageNamed:@"back_to_menu.png"] forState:UIControlStateNormal];
	[self.view addSubview:self.btnMenu];
	
	self.view.backgroundColor = [UIColor colorWithPatternImage:self.backgroundsArr[0]];
}

-(IBAction)pushMusic:(id)sender
{
    return;
}

-(IBAction)pushSound:(id)sender
{
    return;
}

-(IBAction)pushReset:(id)sender
{
    [[ESMSaveManager sharedInstance] clearSave];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(IBAction)pushForParents:(id)sender{
    
}

-(IBAction)pushAbout:(id)sender{
    return;
}

-(IBAction)pushToMenu:(id)sender{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - Getters

- (NSArray *)backgroundsArr
{
	
	static NSArray *imagesArr;
	static dispatch_once_t onceToken;
	
	dispatch_once(&onceToken, ^{
		
		NSArray *arrPaths = [[[ESMFileManager sharedInstance] objectWithName:@"Images.plist"] objectForKey:@"Backgrounds"];
		
		imagesArr = [NSArray new];
		
		for (NSString *imagePath in arrPaths) {
			
			imagesArr  = [imagesArr arrayByAddingObject:[UIImage imageNamed:imagePath]];
			
		}
		
	});
	
	return imagesArr;
	
}

- (void)didTouchMenu
{
	
	[self.navigationController popViewControllerAnimated:YES];
	
}

- (void)reset
{

    [[ESMSaveManager sharedInstance] clearSave];
	
}

@end
