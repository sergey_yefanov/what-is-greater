//
//  SettingsViewController.h
//  Casino
//
//  Created by Sergey Yefanov on 04.08.14.
//  Copyright (c) 2014 Sergey Yefanov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ESMLevelVC.h"

@class ESMLevelVC;

@interface ESMSettingsVC : UIViewController{
    
}

@property(nonatomic, retain) ESMLevelVC *parent;

-(IBAction)pushMusic:(id)sender;
-(IBAction)pushSound:(id)sender;
-(IBAction)pushReset:(id)sender;
-(IBAction)pushForParents:(id)sender;
-(IBAction)pushAbout:(id)sender;
-(IBAction)pushToMenu:(id)sender;

@end
