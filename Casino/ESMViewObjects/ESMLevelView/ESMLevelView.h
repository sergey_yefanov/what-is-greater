//
//  ESMLevelView.h
//  Casino
//
//  Created by Ефанов Сергей on 08.08.14.
//
//

#import <UIKit/UIKit.h>

@protocol ESMLevelViewDelegate;

@interface ESMLevelView : UIView

@property (nonatomic)		NSUInteger					levelIndex;
@property (nonatomic)		NSString *					levelName;
@property (nonatomic)		NSUInteger					percent;
@property (nonatomic)		BOOL						openned;
@property (nonatomic, weak) id <ESMLevelViewDelegate>	delegate;

- (void)update;

@end

@protocol ESMLevelViewDelegate <NSObject>

- (void)didTouchPlay:(ESMLevelView *)levelView;
- (void)didTouchPayFromKoshel:(ESMLevelView *)levelView;
- (void)didTouchPayFromApple:(ESMLevelView *)levelView;
- (void)didTouchRestorePurchase:(ESMLevelView *)levelView;


@end
