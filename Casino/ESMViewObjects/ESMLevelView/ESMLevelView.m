//
//  ESMLevelView.m
//  Casino
//
//  Created by Ефанов Сергей on 08.08.14.
//
//

#import "ESMLevelView.h"
#import "ESMStarView.h"
#import "ESMKoshelFrontView.h"
#import "ESMKoshelBackView.h"

@interface ESMLevelView ()

@property (nonatomic) UILabel  *		labelLevelNumber;
@property (nonatomic) UILabel  *		labelLevelName;
@property (nonatomic) UIButton *		btnPlay;
@property (nonatomic) UIButton *		btnCostFromKoshel;
@property (nonatomic) UIButton *		btnCostFromApple;
@property (nonatomic) UIButton *        btnRestorePurchase;
@property (nonatomic) ESMStarView *		starView;

@end

@implementation ESMLevelView

#pragma mark - Initialization

- (instancetype)init
{
	
    if (self = [super init]) {
		
		[self initialization];
		
    }
	
    return self;
	
}

- (instancetype)initWithFrame:(CGRect)frame
{
    
    if (self = [super initWithFrame:frame]) {

		[self initialization];
		
    }
	
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    
    if (self = [super initWithCoder:aDecoder]) {
		
		[self initialization];
		
    }
	
    return self;
}

- (void)initialization
{
	
	for (UIView *subView in self.subviews) {
		
		[subView removeFromSuperview];
		
	}
	
	// level number label
	
	self.labelLevelNumber = [[UILabel alloc] initWithFrame:CGRectMake(10, 150, 1004, 110)];
	self.labelLevelNumber.textAlignment = NSTextAlignmentCenter;
	self.labelLevelNumber.textColor = [UIColor whiteColor];
	NSArray *fonts = [UIFont fontNamesForFamilyName:@"Nautilus Pompilius"];
	self.labelLevelNumber.font = [UIFont fontWithName:fonts.firstObject size:100.];
	self.labelLevelNumber.shadowColor = [UIColor blackColor];
	self.labelLevelNumber.shadowOffset = CGSizeMake(0.3f, 0.3f);
	self.labelLevelNumber.text = @"";
	self.labelLevelNumber.numberOfLines = 1;
	[self addSubview:self.labelLevelNumber];
	
	// level name label
	
	self.labelLevelName = [[UILabel alloc] initWithFrame:CGRectMake(10, 230, 1004, 250)];
	self.labelLevelName.textAlignment = NSTextAlignmentCenter;
	self.labelLevelName.contentMode = UIViewContentModeCenter;
	self.labelLevelName.font = [UIFont fontWithName:fonts.firstObject size:70.];
	self.labelLevelName.shadowColor = [UIColor blackColor];
	self.labelLevelName.shadowOffset = CGSizeMake(0.3f, 0.3f);
	self.labelLevelName.textColor = [UIColor whiteColor];
	self.labelLevelName.text = @"";
	self.labelLevelName.numberOfLines = 2;
	[self addSubview:self.labelLevelName];
	
	// button start
	self.btnPlay = [UIButton buttonWithType:UIButtonTypeCustom];
	self.btnPlay.frame = CGRectMake(400, 450, 230, 80);
	[self.btnPlay setBackgroundImage:[UIImage imageNamed:@"4_2.png"] forState:UIControlStateNormal];
	[self.btnPlay setTitle:NSLocalizedString(@"Play", nil) forState:UIControlStateNormal];
	[self.btnPlay setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[self.btnPlay addTarget:self action:@selector(pushPlay) forControlEvents:UIControlEventTouchUpInside];
	self.btnPlay.alpha = 0.0f;
	[self addSubview:self.btnPlay];
	
	// button cost from koshel
	self.btnCostFromKoshel = [UIButton buttonWithType:UIButtonTypeCustom];
	self.btnCostFromKoshel.frame = CGRectMake(400, 450, 230, 80);
	[self.btnCostFromKoshel setBackgroundImage:[UIImage imageNamed:@"4_2.png"] forState:UIControlStateNormal];
	[self.btnCostFromKoshel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[self.btnCostFromKoshel addTarget:self action:@selector(pushPayFromKoshel) forControlEvents:UIControlEventTouchUpInside];
	self.btnCostFromKoshel.alpha = 0.0f;
	[self addSubview:self.btnCostFromKoshel];
	
	// button cost from apple
	self.btnCostFromApple = [UIButton buttonWithType:UIButtonTypeCustom];
	self.btnCostFromApple.frame = CGRectMake(170, 450, 320, 80);
	[self.btnCostFromApple setBackgroundImage:[UIImage imageNamed:@"4_2.png"] forState:UIControlStateNormal];
	[self.btnCostFromApple setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[self.btnCostFromApple addTarget:self action:@selector(pushPayFromApple) forControlEvents:UIControlEventTouchUpInside];
	self.btnCostFromApple.alpha = 0.0f;
	[self addSubview:self.btnCostFromApple];
    
    // button Restore Purchase
    self.btnRestorePurchase = [UIButton buttonWithType:UIButtonTypeCustom];
    self.btnRestorePurchase.frame = CGRectMake(550, 450, 320, 80);
    [self.btnRestorePurchase setBackgroundImage:[UIImage imageNamed:@"4_2.png"] forState:UIControlStateNormal];
    [self.btnRestorePurchase setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.btnRestorePurchase addTarget:self action:@selector(pushRestorePurchase) forControlEvents:UIControlEventTouchUpInside];
    self.btnRestorePurchase.alpha = 0.0f;
    [self addSubview:self.btnRestorePurchase];
	
	// stars view
	
	self.starView = [[ESMStarView alloc] initWithFrame:CGRectMake(340, 30, 350, 100)];
	[self addSubview:self.starView];
	
}

#pragma mark - Update

- (void)update
{
	
	NSDictionary *saveData = [[ESMSaveManager sharedInstance] loadSaveAtLevel:self.levelIndex];
	self.labelLevelNumber.text = [NSString stringWithFormat:NSLocalizedString(@"Level %u", nil), self.levelIndex + 1];
	
	if (saveData[@"Opened"]) {
		
		NSUInteger myProgress = ((NSNumber *)saveData[@"MyProgress"]).integerValue;
		NSUInteger maxPragress = ((NSNumber *)saveData[@"MaxProgress"]).integerValue;
		
		self.starView.percent = [self calculatePercentWithProgress:myProgress
													andMaxProgress:maxPragress];
		
		[UIView animateWithDuration:1.0f
						animations:^{
							
							self.btnPlay.alpha = 1.0f;
							self.btnCostFromApple.alpha = 0.0f;
                            self.btnRestorePurchase.alpha = 0.0f;
							self.btnCostFromKoshel.alpha = 0.0f;
							
						}];
		
	} else {
		
		self.starView.percent = 0;
		NSInteger cost = ((NSNumber *)saveData[@"Cost"]).integerValue;
		[self.btnCostFromKoshel setTitle:[NSString stringWithFormat:NSLocalizedString(@"Open with %u coins", nil), (long)cost] forState:UIControlStateNormal];
		[self.btnCostFromApple setTitle:[NSString stringWithFormat:NSLocalizedString(@"Get full version", nil) ] forState:UIControlStateNormal];
        [self.btnRestorePurchase setTitle:[NSString stringWithFormat:NSLocalizedString(@"Restore Purchases", nil) ] forState:UIControlStateNormal];
		
		self.btnPlay.alpha = 0.0f;
		
		if(self.levelIndex == 2 && ![[ESMSaveManager sharedInstance] isFull]) {
		
			self.btnCostFromApple.alpha = 1.0f;
            self.btnRestorePurchase.alpha = 1.0f;
			self.btnCostFromKoshel.alpha = 0.0f;
			
		} else {
			
			self.btnCostFromApple.alpha = 0.0f;
            self.btnRestorePurchase.alpha = 0.0f;
			self.btnCostFromKoshel.alpha = 1.0f;
			
		}
		
	}
	
}

#pragma mark - Setters

- (void)setLevelName:(NSString *)levelName
{
	
	_levelName = levelName;
	self.labelLevelName.text = levelName;
	
}

- (void)setLevelIndex:(NSUInteger)levelIndex
{
	
	_levelIndex = levelIndex;
	[self update];
	
}

#pragma mark - Getters 

- (NSInteger)calculatePercentWithProgress:(NSUInteger)progress
						   andMaxProgress:(NSUInteger)maxProgress
{
	
    if(!maxProgress)
        return 0;
	
    return (float)progress / (float)maxProgress * 100;
	
}

#pragma mark - selectors

- (void)pushPlay
{
	
	if ([self.delegate respondsToSelector:@selector(didTouchPlay:)]) {
		
		[self.delegate didTouchPlay:self];
		
	} else {
		
		NSLog(@"delegate have not selector didTouchPlay:");
		
	}
	
}

- (void)pushPayFromKoshel
{
	
	if ([self.delegate respondsToSelector:@selector(didTouchPayFromKoshel:)]) {
		
		[self.delegate didTouchPayFromKoshel:self];
		
	} else {
		
		NSLog(@"delegate have not selector didTouchPayFromKoshel:");
		
	}
	
}

- (void)pushPayFromApple
{
	
	if ([self.delegate respondsToSelector:@selector(didTouchPayFromApple:)]) {
		
		[self.delegate didTouchPayFromApple:self];
		
	} else {
		
		NSLog(@"delegate have not selector didTouchPayFromApple:");
		
	}
	
}

- (void)pushRestorePurchase
{
    
    if ([self.delegate respondsToSelector:@selector(didTouchRestorePurchase:)]) {
        
        [self.delegate didTouchRestorePurchase:self];
        
    } else {
        
        NSLog(@"delegate have not selector didTouchRestorePurchase:");
        
    }
    
}

@end
