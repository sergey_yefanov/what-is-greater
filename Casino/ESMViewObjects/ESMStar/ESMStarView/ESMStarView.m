//
//  ESMStarView.m
//  Casino
//
//  Created by Ефанов Сергей on 05.08.14.
//
//

#import "ESMStarView.h"

static NSUInteger const MAX_STARS					= 5;

static NSString * const NORMAL_STAR_ENABLE_NAME		= @"star.png";
static NSString * const NORMAL_STAR_FIFTY_NAME		= @"fiftystar.png";
static NSString * const NORMAL_STAR_DISABLE_NAME	= @"grayStar.png";

static NSString * const SMALL_STAR_ENABLE_NAME		= @"Smallstar.png";
static NSString * const SMALL_STAR_FIFTY_NAME		= @"Smallfiftystar.png";
static NSString * const SMALL_STAR_DISABLE_NAME		= @"graySmallStar.png";

typedef UIImageView ESMStar;

typedef NS_ENUM (NSUInteger, ESMStarSize) {
	
	ESMStarSizeNormal,
	ESMStarSizeSmall
	
};

@interface ESMStarView ()

@property (nonatomic) ESMStarSize	starSize;
@property (nonatomic) NSArray *		starsArr;
@property (nonatomic) CGFloat		countStar;

@end

@implementation ESMStarView

#pragma mark - initialization

- (instancetype)init
{
	
	if (self = [super init]) {
		
		[self initialization];
		
	}
	
	return self;
	
}

- (instancetype)initWithFrame:(CGRect)frame
{

    if (self = [super initWithFrame:frame]) {

		[self initialization];
		
    }
	
    return self;
	
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
	
	if (self = [super initWithCoder:aDecoder]) {
		
		[self initialization];
		
	}
	
	return self;
	
}

- (void)initialization
{
	self.backgroundColor = [UIColor clearColor];
	[self createStarsWithNumber:MAX_STARS];
	
}

- (void)createStarsWithNumber:(NSUInteger)countStars
{
	
	NSUInteger width = self.frame.size.width;
	UIImage *starImage = [UIImage imageNamed:NORMAL_STAR_ENABLE_NAME];
	NSUInteger gap;
	
	NSUInteger preGap = width / countStars;
	NSUInteger starImageWidth = starImage.size.width;
	
	if (preGap < starImageWidth) {
		
		starImage = [UIImage imageNamed:SMALL_STAR_ENABLE_NAME];
		self.starSize = ESMStarSizeSmall;
		gap = (width - (starImage.size.width * countStars)) / (countStars - 1) + starImage.size.width;
		
	} else {
		
		self.starSize = ESMStarSizeNormal;
		gap = (width - (starImage.size.width * countStars)) / (countStars - 1) + starImage.size.width;
	}
	
	self.starsArr = [NSArray new];
    
    for (NSUInteger i = 0; i < countStars; i++) {
        
        ESMStar *star = [[ESMStar alloc] initWithImage:starImage];
		star.contentMode = UIViewContentModeCenter;
		star.frame = CGRectMake(i * gap, 0, starImage.size.width, starImage.size.height);
		[self addSubview:star];
        self.starsArr = [self.starsArr arrayByAddingObject:star];
		
    }
	
}

#pragma mark - setters

- (void)setPercent:(NSUInteger)percent
{
    
    if(percent > 100)
        return;
    
    _percent = percent;
    
    if(percent == 0)
        self.countStar = 0.;
    else{
        if(percent == 100)
            self.countStar = 5.;
        else
            self.countStar = ((float)(percent / 10 + 1)) / 2;
    }

}

- (void)setCountStar:(CGFloat)countStar
{
    
    if (countStar < 0 || countStar > 5) {
		
        return;
		
	}
    
    for(NSInteger i = 0; i < self.starsArr.count; i++) {
		
        ESMStar *star = self.starsArr[i];
        
        if(i + 1 <= countStar) {
			
            star.image = [self p_enableStar];
			
		} else {
			
            if(i + 0.5 == _countStar) {
				
                star.image = [self p_fiftyStar];
				
			} else {
				
                star.image = [self p_disableStar];
				
			}
        }
    }
	
}

- (void)show
{
	
    self.alpha = 1.0;
	
}

- (void)hide
{
	
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5f];
    [UIView setAnimationCurve:UIViewAnimationCurveLinear];
    [UIView setAnimationDelegate:self];
    self.alpha = 0.0;
    [UIView commitAnimations];
	
}

#pragma mark - star images

- (UIImage *)p_enableStar{
	 
    if(self.starSize == ESMStarSizeNormal) {
		
        return [UIImage imageNamed:NORMAL_STAR_ENABLE_NAME];
		
	} else if(self.starSize == ESMStarSizeSmall) {
		
        return [UIImage imageNamed:SMALL_STAR_ENABLE_NAME];
		
	}
	
	return nil;
	
}

- (UIImage *)p_fiftyStar {
	
    if(self.starSize == ESMStarSizeNormal) {
		
        return [UIImage imageNamed:NORMAL_STAR_FIFTY_NAME];
		
	} else if(self.starSize == ESMStarSizeSmall) {
		
        return [UIImage imageNamed:SMALL_STAR_FIFTY_NAME];
		
	}
	
	return nil;
	
}

- (UIImage *)p_disableStar {
	
    if(self.starSize == ESMStarSizeNormal) {
		
        return [UIImage imageNamed:NORMAL_STAR_DISABLE_NAME];
		
	} else if(self.starSize == ESMStarSizeSmall) {
		
        return [UIImage imageNamed:SMALL_STAR_DISABLE_NAME];
		
	}
	
	return nil;
	
}


@end
