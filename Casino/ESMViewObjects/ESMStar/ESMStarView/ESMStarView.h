//
//  ESMStarView.h
//  Casino
//
//  Created by Ефанов Сергей on 05.08.14.
//
//

#import <UIKit/UIKit.h>

@interface ESMStarView : UIView

@property (nonatomic) NSUInteger percent;

@end
