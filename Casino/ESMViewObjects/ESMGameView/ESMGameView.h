//
//  ESMGameView.h
//  Casino
//
//  Created by Ефанов Сергей on 06.08.14.
//
//

#import <UIKit/UIKit.h>

typedef NS_ENUM (NSUInteger, ESMGameViewSign) {
	
	ESMGameViewSignMore,
	ESMGameViewSignLess
	
};

typedef NS_ENUM (NSUInteger, ESMGameViewState) {
	
	ESMGameViewStateGame,
	ESMGameViewStateGameTrainer
	
};

@protocol ESMGameViewDelegate;

@interface ESMGameView : UIView 

@property (nonatomic)		NSDictionary *				data;
@property (nonatomic, weak) id <ESMGameViewDelegate>	delegate;
@property (nonatomic)		NSUInteger					numberCoinsInKoshel;
@property (nonatomic)		NSUInteger					percent;
@property (nonatomic)		ESMGameViewState			gameState;

/*
 * Blink symbols
 */

- (void)blinkSymbolAtIndex:(NSUInteger)symbolIndex;
- (void)blinkSymbolsAtIndexes:(NSArray *)symbolsIndexes;
- (void)blinkAllSymbols;

- (void)openSymbolAtIndex:(NSUInteger)symbolIndex;
- (void)openSymbolsAtIndexes:(NSArray *)symbolsIndexes;
- (void)openAllSymbols;

- (void)moveKartAtIndex:(NSUInteger)kartIndex;
- (void)moveKartsAtIndexes:(NSArray *)kartIndexes;
- (void)moveAllKarts;

/*
 * Coins
 */

- (void)hideCoin;
- (void)hideAllCoins;
- (void)moveAllCoinsToKoshel;

/*
 * Signs
 */

- (void)hideLess;
- (void)hideMore;
- (void)sign:(ESMGameViewSign)sign isCorrect:(BOOL)isCorrect;

/*
 * Expressions
 */

- (void)replaceExpressionsWithLeftPart:(NSString *)leftPart
						  andRightPart:(NSString *)rightPart;

/*
 *	Button next
 */

- (void)showButtonNext;
- (void)hideButtonNext;

/*
 * Win status
 */

- (void)showWinStatus;
- (void)showWinStatusWithCoins:(NSUInteger)numberCoins;
- (void)showFailStatusWithMorePart:(NSString *)morePart;
- (void)hideTactics;

@end

@protocol ESMGameViewDelegate <NSObject>

- (void)gameViewDidTouchNextExpression:(ESMGameView *)gameView;
- (void)gameView:(ESMGameView *)gameView didTouchSymbolAtIndex:(NSUInteger)index;
- (void)gameView:(ESMGameView *)gameView didTouchSign:(ESMGameViewSign) sign;
- (void)gameViewDidTouchWin:(ESMGameView *)gameView;

@end
