//
//  ESMGameView.m
//  Casino
//
//  Created by Ефанов Сергей on 06.08.14.
//
//

#import "ESMGameView.h"
#import "ESMStatusView.h"
#import "ESMStarView.h"
#import "ESMKoshelFrontView.h"
#import "ESMKoshelBackView.h"
#import "ESMExpressionView.h"
#import "ESMCoinView.h"
#import "ESMButtonNextExpression.h"
#import "ESMSaveManager.h"
#import "ESMPayManager.h"

@interface ESMGameView () <ESMStatusDelegate, ButtonNextExpressonDelegate, ESMExpressionViewDelegate>

@property (nonatomic) UIView *					tacticsView;
@property (nonatomic) ESMExpressionView *		expressionView;
@property (nonatomic) ESMCoinView *				coinsView;
@property (nonatomic) ESMStatusView *			statusView;
@property (nonatomic) ESMKoshelFrontView *		koshelFrontView;
@property (nonatomic) ESMKoshelBackView *		koshelBackView;
@property (nonatomic) ESMStarView *				starView;
@property (nonatomic) ESMButtonNextExpression *	nextButtonView;
@property (nonatomic) UIButton *				btnFullVersion;
@property (nonatomic) UIButton *				btnRestorePurchase;

@end

@implementation ESMGameView

#pragma mark - initialization

- (instancetype)init
{
	
	if (self = [super init]) {
		
		[self initialization];
		
    }
	
    return self;
	
}

- (instancetype)initWithFrame:(CGRect)frame
{

    if (self = [super initWithFrame:frame]) {

		[self initialization];
		
    }
	
    return self;
	
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
	
    if (self = [super initWithCoder:aDecoder]) {
		
		[self initialization];
		
    }
	
    return self;
	
}

- (void)initialization
{
	
	for (UIView *subView in self.subviews) {
	
		[subView removeFromSuperview];
		
	}
	
	// status view
	
	self.statusView = [[ESMStatusView alloc] initWithFrame:CGRectMake(10, 140, 1004, 100)];
	self.statusView.delegate = self;
	[self.statusView purpose];
	[self addSubview:self.statusView];
	
	// tactics view
	
	self.tacticsView = [[UIView alloc] initWithFrame:CGRectMake(10, 260, 1004, 100)];
	
	UILabel *tacticsLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, -15, self.tacticsView.frame.size.width, self.tacticsView.frame.size.height)];
	tacticsLabel.contentMode = UIViewContentModeCenter;
	tacticsLabel.textAlignment = NSTextAlignmentCenter;
	tacticsLabel.numberOfLines = 2;
    tacticsLabel.text = NSLocalizedString(@"tactics", @"");
	
	UIImageView *tacticsImage = [[UIImageView alloc] initWithFrame:self.tacticsView.bounds];
	tacticsImage.image = [UIImage imageNamed:@"tactics_pl.png"];
	tacticsImage.contentMode = UIViewContentModeCenter;
	self.tacticsView.backgroundColor = [UIColor clearColor];
	[self.tacticsView addSubview:tacticsImage];
	[self.tacticsView addSubview:tacticsLabel];
	
	[self addSubview:self.tacticsView];
	
	// Expression view
	
	self.expressionView = [[ESMExpressionView alloc] initWithFrame:CGRectMake(10, 350, 1004, 140)];
	self.expressionView.delegate = self;
	[self addSubview:self.expressionView];
	
	// koshel back view
	
	self.koshelBackView = [[ESMKoshelBackView alloc] initWithFrame:CGRectMake(0, 540, 300, 200)];
	[self addSubview:self.koshelBackView];
	
	// full version
	self.btnFullVersion = [UIButton buttonWithType:UIButtonTypeCustom];
	self.btnFullVersion.frame = CGRectMake(170, 440, 320, 80);
	[self.btnFullVersion setBackgroundImage:[UIImage imageNamed:@"4_2.png"] forState:UIControlStateNormal];
	[self.btnFullVersion setTitle:NSLocalizedString(@"Get full version", nil) forState:UIControlStateNormal];
	[self.btnFullVersion setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[self.btnFullVersion addTarget:self action:@selector(pushFullVersion) forControlEvents:UIControlEventTouchUpInside];
	[self addSubview:self.btnFullVersion];
	
	// restore purchases
	self.btnRestorePurchase = [UIButton buttonWithType:UIButtonTypeCustom];
	self.btnRestorePurchase.frame = CGRectMake(550, 440, 320, 80);
	[self.btnRestorePurchase setBackgroundImage:[UIImage imageNamed:@"4_2.png"] forState:UIControlStateNormal];
	[self.btnRestorePurchase setTitle:NSLocalizedString(@"Restore Purchases", nil) forState:UIControlStateNormal];
	[self.btnRestorePurchase setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[self.btnRestorePurchase addTarget:self action:@selector(pushRestorePurchases) forControlEvents:UIControlEventTouchUpInside];
	[self addSubview:self.btnRestorePurchase];
	
	// coins view
	
	self.coinsView = [[ESMCoinView alloc] initWithFrame:CGRectMake(395, 540, 300, 200)];
	NSNumber *numberCoins = self.data[@"ESM_GAME_LOADED_EXPRESSION_NUMBER_CLOSE_DIGITS_KEY"];
	[self.coinsView createCoins:numberCoins.integerValue + 1
				withOnePosition:NO];
	[self addSubview:self.coinsView];
	
	// koshel front view
	
	self.koshelFrontView = [[ESMKoshelFrontView alloc] initWithFrame:CGRectMake(0, 540, 300, 200)];
	self.koshelFrontView.countCoins = self.numberCoinsInKoshel = ((NSNumber *)self.data[@"ESM_GAME_LOADED_COINS_IN_COSHEL"]).integerValue;
	[self addSubview:self.koshelFrontView];
	
	// stars view
	
	self.starView = [[ESMStarView alloc] initWithFrame:CGRectMake(340, 30, 350, 100)];
	[self addSubview:self.starView];
	
	// button next
	
	self.nextButtonView = [[ESMButtonNextExpression alloc] initWithFrame:CGRectMake(700, 540, 300, 200)];
	self.nextButtonView.delegate = self;
	[self addSubview:self.nextButtonView];
	
	if (self.gameState == ESMGameViewStateGameTrainer) {
		
		self.tacticsView.hidden = YES;
		self.koshelBackView.hidden = YES;
		self.koshelFrontView.hidden = YES;
		self.coinsView.hidden = YES;
		self.starView.hidden = YES;
		
		if ([[ESMSaveManager sharedInstance] trainerCount] >= 7 && ![[ESMSaveManager sharedInstance] isFull]) {
			
			self.expressionView.hidden = YES;
			self.btnFullVersion.hidden = NO;
			self.btnRestorePurchase.hidden = NO;
			[self.statusView fullVersion];
            [Flurry logEvent:@"TRAINER_SHOWED_GET_FULL"];
			
		} else {
			
			self.expressionView.hidden = NO;
			self.btnFullVersion.hidden = YES;
			self.btnRestorePurchase.hidden = YES;
			
		}
		
	} else {
		
		self.btnFullVersion.hidden = YES;
		self.btnRestorePurchase.hidden = YES;
		
	}
	
	__weak ESMGameView *weakSelf = self;
	
	__block id observer = [[NSNotificationCenter defaultCenter] addObserverForName:ESM_COINS_MOVED_NOTIFICATION
													  object:[self.coinsView class]
													   queue:nil
												  usingBlock:^(NSNotification *note) {
													  
													  weakSelf.koshelFrontView.countCoins = weakSelf.numberCoinsInKoshel;
													  [weakSelf.koshelFrontView close];
													  [weakSelf.koshelBackView close];
													  [[NSNotificationCenter defaultCenter] removeObserver:observer];
		
												  }];
	
	
}

#pragma mark - Setters

- (void)setGameState:(ESMGameViewState)gameState
{
	
	_gameState = gameState;
	
	if (self.gameState == ESMGameViewStateGameTrainer) {
		
		self.tacticsView.hidden = YES;
		self.koshelBackView.hidden = YES;
		self.koshelFrontView.hidden = YES;
		self.coinsView.hidden = YES;
		
	}
	
}

- (void)setPercent:(NSUInteger)percent
{
	
	_percent = percent;
	self.starView.percent = percent;
	
}

- (void)setData:(NSDictionary *)data
{
	
	_data = data;
	
	[self initialization];
	
	NSDictionary *expressionData = @{@"LeftPart" : data[@"ESM_GAME_LOADED_EXPRESSION_LEFT_PART_KEY"],
									 @"RightPart" : data[@"ESM_GAME_LOADED_EXPRESSION_RIGHT_PART_KEY"]};
	
	self.expressionView.data = expressionData;
	self.percent = ((NSNumber *)data[@"ESM_GAME_LOADED_PERCENT"]).integerValue;
	
	[self.nextButtonView hide];
	
}

- (void)setNumberCoinsInKoshel:(NSUInteger)numberCoinsInKoshel
{
	
	_numberCoinsInKoshel = numberCoinsInKoshel;
	
}

#pragma mark - Blink symbols

- (void)blinkSymbolAtIndex:(NSUInteger)symbolIndex
{
	
	[self.expressionView blinkSymbolAtIndex:symbolIndex];
	
}

- (void)blinkSymbolsAtIndexes:(NSArray *)symbolsIndexes
{
	
	[self.expressionView blinkSymbolsAtIndexes:symbolsIndexes];
	
}

- (void)blinkAllSymbols
{
	
	[self.expressionView blinkAllSymbols];
	
}

#pragma mark - Open symbols

- (void)openSymbolAtIndex:(NSUInteger)symbolIndex
{
	
	[self.expressionView openSymbolAtIndex:symbolIndex];
	
}

- (void)openSymbolsAtIndexes:(NSArray *)symbolsIndexes
{
	
	[self.expressionView openSymbolsAtIndexes:symbolsIndexes];
	
}

- (void)openAllSymbols
{
	
	[self.expressionView openAllSymbols];
	
}

#pragma mark - Tactics

- (void)hideTactics
{
	
	[UIView animateWithDuration:1.0f
					  animations:^{
						  
						  self.tacticsView.alpha = 0.0f;
						  
					  } completion:nil];
	
}

#pragma mark - Move Karts

- (void)moveKartAtIndex:(NSUInteger)kartIndex
{
	
	[self.expressionView moveKartAtIndex:kartIndex];
	
}

- (void)moveKartsAtIndexes:(NSArray *)kartIndexes
{

	[self.expressionView moveKartsAtIndexes:kartIndexes];
	
}

- (void)moveAllKarts
{
	
	[self.expressionView moveAllKarts];
	
}


#pragma mark - Coins

- (void)hideCoin
{
	
	[self.coinsView hideCoin];
	
}

- (void)hideAllCoins
{
	
	[self.coinsView hideAllCoins];
	
}

- (void)moveAllCoinsToKoshel
{
	[self.koshelFrontView open];
	[self.koshelBackView open];
	[self.coinsView moveAllCoinsToKoshel];
	
}

#pragma mark - Signs

- (void)hideLess
{
	
    [self.expressionView hideLess];
	
}

- (void)hideMore
{
	
	[self.expressionView hideMore];
	
}

- (void)sign:(ESMGameViewSign)sign isCorrect:(BOOL)isCorrect
{
	
	ESMExpressionViewSign expressionViewSign;
	
	if (sign == ESMGameViewSignLess) {
		
		expressionViewSign = ESMExpressionViewSignLess;
		
	} else {
		
		expressionViewSign = ESMExpressionViewSignMore;
		
	}
	
	[self.expressionView sign:expressionViewSign
					isCorrect:isCorrect];
	
}

#pragma mark - Expressions

- (void)replaceExpressionsWithLeftPart:(NSString *)leftPart
						  andRightPart:(NSString *)rightPart
{
	
	[self.expressionView replaceExpressionsWithLeftPart:leftPart
										   andRightPart:rightPart];
	
}

#pragma mark - Button next

- (void)showButtonNext
{
	
	[self.nextButtonView show];
	
}

- (void)hideButtonNext
{
	
	[self.nextButtonView hide];
	
}

#pragma mark - Win status

- (void)showWinStatus
{
	
	[self.statusView win];
	
}

- (void)showWinStatusWithCoins:(NSUInteger)numberCoins
{
	
	[self.statusView winWithCoins:numberCoins];
	
}

- (void)showFailStatusWithMorePart:(NSString *)morePart
{
	
	[self.statusView loseWithMorePart:morePart];
	
}

#pragma mark - Next level process delegate

- (void)didTouchNextExpression
{
	
	if ([self.delegate respondsToSelector:@selector(gameViewDidTouchNextExpression:)]) {
		
		[self.delegate gameViewDidTouchNextExpression:self];
		
	} else {
		
		NSLog(@"delegate not have selector gameViewDidTouchNextExpression:");
		
	}
	
}

#pragma mark - Expression view delegate

- (void)expressionView:(ESMExpressionView *)expressionView didTouchSign:(ESMExpressionViewSign)sign
{
	
	if ([self.delegate respondsToSelector:@selector(gameView:didTouchSign:)]) {
		
		ESMGameViewSign gameViewSign;
		
		if (sign == ESMExpressionViewSignLess) {
			
			gameViewSign = ESMGameViewSignLess;
			
		} else {
			
			gameViewSign = ESMGameViewSignMore;
			
		}
	
		[self.delegate gameView:self didTouchSign:gameViewSign];
		
	} else {
		
		NSLog(@"delagate have not selector gameView:didTouchSign:");
		
	}
	
}

- (void)expressionView:(ESMExpressionView *)expressionView didTouchSymbolAtIndex:(NSUInteger)index
{
	
	if ([self.delegate respondsToSelector:@selector(gameView:didTouchSymbolAtIndex:)]) {
		
		[self.delegate gameView:self didTouchSymbolAtIndex:index];
		
	} else {
		
		NSLog(@"delagate have not selector gameView:didTouchSymbolAtIndex:");
		
	}
	
}

#pragma mark - Win status delegate

- (void)didTouchStatus:(UIView *)view
{
	
	[self blinkSymbolsAtIndexes:nil];
	
}

- (void)pushFullVersion
{
	[SVProgressHUD setBackgroundColor:[UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:0.5]];
	[SVProgressHUD setForegroundColor:[UIColor whiteColor]];
	[SVProgressHUD show];
    [Flurry logEvent:@"TRAINER_TOUCH_GET_FULL"];
	
	__block id observerTransaction =  [[NSNotificationCenter defaultCenter] addObserverForName:ESM_TRANSACTION_FALIED
																			 object:nil
																			  queue:nil
																		 usingBlock:^(NSNotification *note) {
																			 
																			 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
																				 // time-consuming task
																				 dispatch_sync(dispatch_get_main_queue(), ^{
																					 [SVProgressHUD dismiss];
																				 });
																			 });
                                                                             
                                                                             [Flurry logEvent:@"TRAINER_GET_FULL_FAIL"];
																			 
																			 [[NSNotificationCenter defaultCenter] removeObserver:observerTransaction];
																			 UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil) message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
																			 [alertView show];
																			 
																		 }];
	
	__block id observer =  [[NSNotificationCenter defaultCenter] addObserverForName:ESM_FULL_VERSION_PURCHASED
																			  object:nil
																			   queue:nil
																		  usingBlock:^(NSNotification *note) {
																			  
																			  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
																				  // time-consuming task
																				  dispatch_async(dispatch_get_main_queue(), ^{
																					  [SVProgressHUD dismiss];
																				  });
																			  });
                                                                              
                                                                              [Flurry logEvent:@"TRAINER_GET_FULL_OK"];
																			  
																			  [[ESMSaveManager sharedInstance] fullOk];
																			  [self didTouchNextExpression];
																			  [[NSNotificationCenter defaultCenter] removeObserver:observer];
																			  
																		  }];
	
	[[ESMPayManager sharedInstance] buyFullVersion];

}

- (void)pushRestorePurchases
{
	NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
								   @"Game", @"place",
								   nil];
	
	[Flurry logEvent:@"GAME_BUY_TOUCH" withParameters:articleParams];
	[SVProgressHUD setBackgroundColor:[UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:0.5]];
	[SVProgressHUD setForegroundColor:[UIColor whiteColor]];
	[SVProgressHUD show];
	
	__block id observerTransaction =  [[NSNotificationCenter defaultCenter] addObserverForName:ESM_TRANSACTION_FALIED
																						object:nil
																						 queue:nil
																					usingBlock:^(NSNotification *note) {
																						
																						dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
																							// time-consuming task
																							dispatch_sync(dispatch_get_main_queue(), ^{
																								[SVProgressHUD dismiss];
																							});
																						});

																						[[NSNotificationCenter defaultCenter] removeObserver:observerTransaction];
																						UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil) message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
																						[alertView show];
																						
																					}];
	
	__block id observer =  [[NSNotificationCenter defaultCenter] addObserverForName:ESM_FULL_VERSION_PURCHASED
																			 object:nil
																			  queue:nil
																		 usingBlock:^(NSNotification *note) {
																		  
																			 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
																				 // time-consuming task
																				 dispatch_async(dispatch_get_main_queue(), ^{
																					 [SVProgressHUD dismiss];
																				 });
																			 });

																			 [[ESMSaveManager sharedInstance] fullOk];
																			 [self didTouchNextExpression];
																			 [[NSNotificationCenter defaultCenter] removeObserver:observer];
																			 
																		 }];
	
	[[ESMPayManager sharedInstance] restorePurchace];
}

@end
