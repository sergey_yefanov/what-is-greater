//
//  ESMCoinView.m
//  Casino
//
//  Created by Ефанов Сергей on 05.08.14.
//
//

static NSUInteger const MAX_COUNT_COIN_IN_LINE	= 5;
static CGFloat    const DURATION_MOVE			= 1.0;
static CGFloat    const DURATION_HIDE			= 0.5;
static CGFloat    const MULTIPLY_DELAY			= 0.5;
static CGFloat	  const GAP_COIN_X				= 58;
static CGFloat	  const GAP_COIN_Y				= 58;
static NSString * const COIN_IMAGE_NAME			= @"moneta.png";

NSString * const ESM_COINS_MOVED_NOTIFICATION	= @"ESM_COINS_MOVED_NOTIFICATION";

typedef UIImageView ESMCoin;

#import "ESMCoinView.h"

@interface ESMCoinView ()

@property (nonatomic) NSMutableArray *coinsArr;
@property (nonatomic) NSUInteger countCoins;

@end

@implementation ESMCoinView

#pragma mark - initialization

- (instancetype)init
{
	
    if(self = [super init]){
		
		[self initialize];
		
    }
    
    return self;
	
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
	
	if(self = [super initWithCoder:aDecoder]) {
		
		[self initialize];
		
	}
	
	return self;
	
}

- (instancetype)initWithFrame:(CGRect)frame
{
	
	if (self = [super initWithFrame:frame]) {
		
		[self initialize];
		
	}
	
	return self;
	
}

- (void)initialize
{
	
	self.coinsArr = [NSMutableArray new];
	self.countCoins = self.coinsArr.count;
	self.backgroundColor = [UIColor clearColor];
	
}

- (void)createCoins:(NSInteger)numberCoins withOnePosition:(BOOL)onePosition
{
	
	self.countCoins = numberCoins;
	UIImage *imageCoin = [UIImage imageNamed:COIN_IMAGE_NAME];
	
	NSInteger numberLines = numberCoins % MAX_COUNT_COIN_IN_LINE ? numberCoins / MAX_COUNT_COIN_IN_LINE + 1 : numberCoins / MAX_COUNT_COIN_IN_LINE;
	
	NSInteger startLineY = (self.frame.size.height - (GAP_COIN_X * (numberLines - 1) + imageCoin.size.height)) / 2;
	
    for (NSUInteger i = 0; i < self.countCoins; i++) {
		
		NSInteger const numberThisLine = (i / MAX_COUNT_COIN_IN_LINE);
		NSInteger const count = (numberThisLine + 1) * MAX_COUNT_COIN_IN_LINE;
		BOOL isFullLine = numberCoins - count > 0;
		NSUInteger const numberCoinsInThisLine = isFullLine? MAX_COUNT_COIN_IN_LINE : numberCoins % MAX_COUNT_COIN_IN_LINE;
		NSUInteger const startLineX = (self.frame.size.width - (GAP_COIN_X * (numberCoinsInThisLine - 1) + imageCoin.size.width)) / 2;
        
		NSInteger x = onePosition ? startLineX : startLineX + i % MAX_COUNT_COIN_IN_LINE * GAP_COIN_X;
		NSInteger y = onePosition ? startLineY : startLineY + numberThisLine * GAP_COIN_Y;
		
		CGRect frame = CGRectMake(x, y, imageCoin.size.width, imageCoin.size.height);
		
        ESMCoin *coin = [[ESMCoin alloc] initWithFrame:frame];
		coin.contentMode = UIViewContentModeCenter;
		coin.image = imageCoin;
		
		[self addSubview:coin];
        
        [self.coinsArr addObject:coin];
    }
}

#pragma mark - move coins

- (void)moveAllCoinsToKoshel
{
	
	__block NSUInteger countMovedCoins = 0;
    
    for (NSInteger i = self.countCoins - 1; i >= 0 ; i--) {
        ESMCoin *coin = self.coinsArr[i];
		
		__weak id weakSelf = self;
		
		[ESMCoin animateWithDuration:0.5
							   delay:MULTIPLY_DELAY * (self.countCoins - i - 1)
							 options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionBeginFromCurrentState
						  animations:^{
							  
							  coin.center = CGPointMake(-120, -50);
							  
						  } completion:^(BOOL finishedOne) {
							  
							  if (finishedOne) {
							  
								  [ESMCoin animateWithDuration:0.5 animations:^{
									  
									  coin.center = CGPointMake (-230, 100);
									  
								  } completion:^(BOOL finishedTwo) {
									  
									  if (finishedTwo) {
									  
										  [ESMCoin animateWithDuration:DURATION_MOVE animations:^{
											  coin.alpha = 0.0f;
										  }];
										  
										  countMovedCoins++;
										  
										  if(countMovedCoins == self.countCoins) {
											  
											  [weakSelf coinsMovedWithCount:countMovedCoins];
											  
										  }
										  
									  }
									  
								  }];
								  
							  }
							  
						  }];
    }
}

- (void)moveAllCoinsToCenter
{
	
	__block NSUInteger countMovedCoins = 0;
	
	for (NSInteger i = self.countCoins - 1; i >= 0 ; i--) {
		ESMCoin *coin = self.coinsArr[i];
		
		__weak id weakSelf = self;
		
		[ESMCoin animateWithDuration:0.4
							   delay:0.4 * (self.countCoins - i - 1)
							 options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionBeginFromCurrentState
						  animations:^{
							  
							  coin.center = CGPointMake(350, -120);
							  
						  } completion:^(BOOL finished) {
							  
							  if (finished) {
								  
								  [ESMCoin animateWithDuration:0.2 animations:^{
									  coin.alpha = 0.0f;
								  }];
								  
								  countMovedCoins++;
								  
								  if(countMovedCoins == self.countCoins) {
									  
									  [weakSelf coinsMovedWithCount:countMovedCoins];
									  
								  }
								  
							  }
							  
						  }];
	}
}

- (void)coinsMovedWithCount:(NSUInteger)countCoins {
	
	self.countCoins = 0;
	[self.coinsArr removeAllObjects];
	
	[[NSNotificationCenter defaultCenter] postNotificationName:ESM_COINS_MOVED_NOTIFICATION object:[self class] userInfo:@{@"countCoin" : @(countCoins)}];
	
}

#pragma mark - remove coins

- (void)hideCoin
{
    
    if (!self.countCoins) {
		
        return;
		
	}
    
    self.countCoins--;
    ESMCoin *coin = [self.coinsArr lastObject];
	
	[ESMCoin animateWithDuration:DURATION_HIDE
					  animations:^{
						  
						  coin.alpha = 0.0f;
						  
					  } completion:^(BOOL finished) {
						  
						  [self.coinsArr removeObject:coin];
						  
					  }];
}

- (void)hideAllCoins
{
    
    if (!self.countCoins) {
		
        return;
		
	}
    
    self.countCoins = 0;
    [self.coinsArr enumerateObjectsUsingBlock:^(ESMCoin *coin, NSUInteger idx, BOOL *stop) {
		
		[ESMCoin animateWithDuration:DURATION_HIDE
						  animations:^{
							  
							  coin.alpha = 0.0f;
							  
						  } completion:^(BOOL finished) {
							  
							  [self.coinsArr removeObject:coin];
							  
						  }];
		
	}];
	
}


@end
