//
//  ESMCoinView.h
//  Casino
//
//  Created by Ефанов Сергей on 05.08.14.
//
//

#import <UIKit/UIKit.h>

extern NSString * const ESM_COINS_MOVED_NOTIFICATION;

@interface ESMCoinView : UIView

@property (nonatomic, readonly) NSUInteger countCoins;

- (void)createCoins:(NSInteger)numberCoins
	withOnePosition:(BOOL)onePosition;

- (void)hideCoin;
- (void)hideAllCoins;
- (void)moveAllCoinsToKoshel;
- (void)moveAllCoinsToCenter;

@end
