//
//  ESMPayManager.m
//  What Is Greater
//
//  Created by Ефанов Сергей on 26.08.14.
//
//

#import "ESMPayManager.h"
#import "IAPHelper.h"

NSString * const ESM_FULL_VERSION_PURCHASED = @"ESM_FULL_VERSION_PURCHASED";
NSString * const ESM_TRANSACTION_FALIED = @"ESM_TRANSACTION_FALIED";

@interface ESMPayManager ()

@property (nonatomic) IAPHelper *iapHelper;

@end

@implementation ESMPayManager

+ (instancetype)sharedInstance
{
	
	static id payManager = nil;
	static dispatch_once_t onceToken;
	
	dispatch_once(&onceToken, ^{
		
		payManager = [self new];
		
	});
	
    return payManager;
	
}

- (void)buyFullVersion
{
    
    [[NSNotificationCenter defaultCenter] addObserverForName:IAPHelperProductPurchasedNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:ESM_FULL_VERSION_PURCHASED object:nil];
        
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:    IAPHelperProductFailedNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:ESM_TRANSACTION_FALIED object:nil];
        
    }];
    
    [self.iapHelper requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
        
        [products enumerateObjectsUsingBlock:^(SKProduct *product, NSUInteger idx, BOOL *stop) {
            
            [self.iapHelper buyProduct:product];
            
        }];
        
    }];

}

- (void) restorePurchace
{
    
    [[NSNotificationCenter defaultCenter] addObserverForName:IAPHelperProductPurchasedNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:ESM_FULL_VERSION_PURCHASED object:nil];
        
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:    IAPHelperProductFailedNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:ESM_TRANSACTION_FALIED object:nil];
        
    }];
    
    [self.iapHelper restoreCompletedTransactions];
    
}

- (IAPHelper *)iapHelper
{
    
    static id iapHelperStatic = nil;
	static dispatch_once_t onceToken;
	
	dispatch_once(&onceToken, ^{
		
		iapHelperStatic = [[IAPHelper alloc] initWithProductIdentifiers:[NSSet setWithObject:@"whatisgreater.sergey.yefanov.ru.fullVersion"]];
		
	});
    
    return iapHelperStatic;
}

@end
