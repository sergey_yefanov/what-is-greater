//
//  ESMPayManager.h
//  What Is Greater
//
//  Created by Ефанов Сергей on 26.08.14.
//
//

#import <Foundation/Foundation.h>

extern NSString * const ESM_FULL_VERSION_PURCHASED;
extern NSString * const ESM_TRANSACTION_FALIED;

@interface ESMPayManager : NSObject

@property (nonatomic, readonly) BOOL fullVersionIsBought;

+ (instancetype)sharedInstance;
- (void) buyFullVersion;
- (void) restorePurchace;

@end
