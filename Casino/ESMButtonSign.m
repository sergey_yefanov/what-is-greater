//
//  ButtonSign.m
//  Casino
//
//  Created by Sergey Yefanov on 04.08.14.
//  Copyright (c) 2014 Sergey Yefanov. All rights reserved.
//

#define CENTER_Y 100
#define LESS_IMAGE          @"sn_m.png"
#define LESS_IMAGE_FALSE    @"sn_m_false.png"
#define MORE_IMAGE          @"sn_b.png"
#define MORE_IMAGE_FALSE    @"sn_b_false.png"

#import "ESMButtonSign.h"

@implementation ESMButtonSign

#pragma mark - Initialization

- (instancetype)init
{
    
    if (self = [super init]) {
		
        [self initialization];
		
    }
	
    return self;
	
}

- (instancetype)initWithImage:(UIImage *)image
{
	if (self = [super initWithImage:image]) {
		
		[self initialization];
		
	}
	
	return self;
	
}

- (instancetype)initWithFrame:(CGRect)frame
{
	if (self = [super initWithFrame:frame]) {
		
		[self initialization];
		
	}
	
	return self;
	
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
	
	if (self = [super initWithCoder:aDecoder]) {
		
		[self initialization];
		
	}
	
	return self;
	
}

- (void)initialization
{
	
	self.exclusiveTouch = YES;
	self.userInteractionEnabled = YES;
	
}

#pragma mark -

//- (uint32_t) getPixel:(CGPoint) point;
//{
//	GLuint components;
//	GLuint imgWide, imgHigh;      // Real image size
//	GLuint rowBytes, rowPixels;   // Image size padded by CGImage
//	CGBitmapInfo info;            // CGImage component layout info
//	CGColorSpaceModel colormodel; // CGImage colormodel (RGB, CMYK, paletted, etc)
//	GLenum internal, format;
//	GLubyte *pixels = NULL;
//	int x1=lrintf(point.x);
//	int y1=lrintf(point.y);
//	//printf("Point = %d : %d \n", x, y);
//	if (! CGRectContainsPoint([self bounds], point))
//	{
//		return 0;
//	}
//	CGImageRef CGImage = [self image].CGImage;
//    //	rt_assert(CGImage);
//	if (!CGImage)
//		return 0;
//	
//	// Parse CGImage info
//	info       = CGImageGetBitmapInfo(CGImage);		// CGImage may return pixels in RGBA, BGRA, or ARGB order
//	colormodel = CGColorSpaceGetModel(CGImageGetColorSpace(CGImage));
//	size_t bpp = CGImageGetBitsPerPixel(CGImage);
//	if (bpp < 8 || bpp > 32 || (colormodel != kCGColorSpaceModelMonochrome && colormodel != kCGColorSpaceModelRGB))
//	{
//		// This loader does not support all possible CGImage types, such as paletted images
//        //		CGImageRelease(CGImage);
//		return 0;
//	}
//    
//	components = bpp>>3;
//	rowBytes   = CGImageGetBytesPerRow(CGImage);	// CGImage may pad rows
//	rowPixels  = rowBytes / components;
//	imgWide    = CGImageGetWidth(CGImage);
//	imgHigh    = CGImageGetHeight(CGImage);
//	//printf("Size= %d : %d \n", imgWide, imgHigh);
//	
//	// Choose OpenGL format
//	switch(bpp)
//	{
//		default:
//            
//		case 32:
//		{
//			internal = GL_RGBA;
//			switch(info & kCGBitmapAlphaInfoMask)
//			{
//				case kCGImageAlphaPremultipliedFirst:
//				case kCGImageAlphaFirst:
//				case kCGImageAlphaNoneSkipFirst:
//					format = GL_BGRA;
//					break;
//				default:
//					format = GL_RGBA;
//			}
//			break;
//		}
//		case 24:
//			internal = format = GL_RGB;
//			break;
//		case 16:
//			internal = format = GL_LUMINANCE_ALPHA;
//			break;
//		case 8:
//			internal = format = GL_LUMINANCE;
//			break;
//	}
//	
//	// Get a pointer to the uncompressed image data.
//	//
//	// This allows access to the original (possibly unpremultiplied) data, but any manipulation
//	// (such as scaling) has to be done manually. Contrast this with drawing the image
//	// into a CGBitmapContext, which allows scaling, but always forces premultiplication.
//	CFDataRef data = CGDataProviderCopyData(CGImageGetDataProvider(CGImage));
//	pixels = (GLubyte *)CFDataGetBytePtr(data);
//	uint32_t pixel = ((uint32_t *)pixels)[y1 * rowPixels + x1];
//	
//	// If the CGImage component layout isn't compatible with OpenGL, fix it.
//	// On the device, CGImage will generally return BGRA or RGBA.
//	// On the simulator, CGImage may return ARGB, depending on the file format.
//	if (format == GL_BGRA)
//	{
//		if ((info & kCGBitmapByteOrderMask) != kCGBitmapByteOrder32Host)
//		{
//			// Convert from ARGB to BGRA
//            pixel = (pixel << 24) | ((pixel & 0xFF00) << 8) | ((pixel >> 8) & 0xFF00) | (pixel >> 24);
//		}
//		
//		// All current iPhoneOS devices support BGRA via an extension.
//	}
//	//printf("Pixel = %x \n", pixel);
//	CFRelease(data);
//	return pixel;
//}
//
//- (BOOL) pointInside:(CGPoint)point withEvent:(UIEvent *)event {
//	return 	0 != (0xFF000000 & [self getPixel:point]);
//}

- (void)moveToCenter{
	
	[UIView animateWithDuration:1.0f
						  delay:0.0f
						options:UIViewAnimationOptionCurveLinear
					 animations:^{
		
		    self.center = CGPointMake(self.frame.origin.x + 25, CENTER_Y);
		
	} completion:^(BOOL finished) {
		
		self.userInteractionEnabled = NO;
		
	}];
	
}

- (void)hide
{
	
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0f];
    [UIView setAnimationCurve:UIViewAnimationCurveLinear];
    [UIView setAnimationDelegate:self];
    self.alpha = 0.0;
    [UIView commitAnimations];
	
}

- (void)setTrue:(BOOL)flag
{
	
    if (flag) {
		
        [self setImage:[UIImage imageNamed:LESS_IMAGE]];
		
	} else {
		
        [self setImage:[UIImage imageNamed:LESS_IMAGE_FALSE]];
		
    }
	
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	
	if ([self.delegate respondsToSelector:@selector(buttonSignDidTouch:)]) {
		
		[self.delegate buttonSignDidTouch:self];
		
	} else {
		
		NSLog(@"delegate not have selector buttonSignDidTouch:");
		
	}
	
}

//-(void) setTrue:(BOOL)flag{
//    if(flag)
//        [self setImage:[UIImage imageNamed:MORE_IMAGE]];
//    else {
//        [self setImage:[UIImage imageNamed:MORE_IMAGE_FALSE]];
//    }
//}

@end
