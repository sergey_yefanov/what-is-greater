//
//  PartOfExpression.m
//  Casino
//
//  Created by Sergey Yefanov on 04.08.14.
//  Copyright (c) 2014 Sergey Yefanov. All rights reserved.
//

#import "ESMPartOfExpressionManager.h"
#import "NSString + Expression.h"

@interface ESMPartOfExpressionManager ()

@property (nonatomic) NSUInteger		numberOfCloseDigit;
@property (nonatomic) NSUInteger		numberOfOpenDigit;
@property (nonatomic) NSInteger			summMin;
@property (nonatomic) NSInteger			summMax;
@property (nonatomic) NSString *		stringOfExpression;
@property (nonatomic) NSString *		stringMax;
@property (nonatomic) NSString *		stringMin;
@property (nonatomic) NSArray *			openedIndexes;
@property (nonatomic) NSArray *			startOpenedIndexes;
@property (nonatomic) NSDictionary *	variantKarta;
@property (nonatomic) NSMutableString *	openStr;

@end

@implementation ESMPartOfExpressionManager

#pragma mark - initialization

- (instancetype)init
{
	
    if (self = [super init]) {
        
		[self initialization];
        
    }
	
    return self;
	
}

- (instancetype)initWithData:(NSDictionary *)data
{
	
    if (self = [self init]) {
		
		[self initialization];
		self.data = data;
		
    }
    
    return self;
	
}

- (void)initialization
{
	
	self.stringOfExpression = @"";
	
}

#pragma mark - Setters

- (void)setData:(NSDictionary *)data
{
	
	self.stringOfExpression = data[@"Expression"];
	self.startOpenedIndexes = [((NSString *)data[@"Opened"]) trueIndexesArray];
	self.openedIndexes = self.startOpenedIndexes.copy;
	self.openStr = ((NSString *)data[@"Opened"]).mutableCopy;
	self.variantKarta = data[@"VariantKarta"];

	NSDictionary* dictVar = self.variantKarta[self.openStr];
	
	self.summMin = [dictVar[@"MIN"][@"Summ"] integerValue];
	self.summMax = [dictVar[@"MAX"][@"Summ"] integerValue];
	self.stringMax = self.variantKarta[self.openStr][@"MAX"][@"Expression"];
	self.stringMin = self.variantKarta[self.openStr][@"MIN"][@"Expression"];
	
}

#pragma mark - Getters

- (NSInteger)numberOfDigit
{
	
    return [self.stringOfExpression numberOfDigit];
	
}

- (NSUInteger)numberOfCloseDigit
{
	
	if ([self.openStr isEqualToString:@"0"]) {
		
		return [self.stringOfExpression numberOfDigit] - self.numberOfOpenDigit;
		
	}
	
	return self.openStr.length - self.numberOfOpenDigit;
	
}

- (NSUInteger)numberOfOpenDigit
{
	
	return self.openedIndexes.count;
	
}

#pragma mark - Process

- (BOOL)openDigitAtIndex:(NSUInteger)index
{
	
	if([self.openStr isEqualToString:@"0"]) {
		
		for(NSUInteger i = 0; i < [self.stringOfExpression numberOfDigit] - 1; i++) {
			
			[self.openStr appendString:@"0"];
			
		}
		
	}
	
	NSUInteger digitNumber = -1;
	
	for (NSUInteger i = 0; i <= index; i++) {
		
		NSString *symbol = [self.stringOfExpression symbolAtIndex:i];
		
		if (symbol.isDigit) {
			
			digitNumber++;
			
		}
		
	}
	
	self.openedIndexes = [self.openedIndexes arrayByAddingObject:@(digitNumber)];
	
	NSRange range;
	range.location = digitNumber;
	range.length = 1;
	
	[self.openStr replaceCharactersInRange:range withString:@"1"];
    
    NSDictionary* dictVariant = self.variantKarta[self.openStr];
    self.summMin = [dictVariant [@"MIN"][@"Summ"] integerValue];
    self.summMax = [dictVariant [@"MAX"][@"Summ"] integerValue];
    self.stringMax = self.variantKarta[self.openStr][@"MAX"][@"Expression"];
    self.stringMin = self.variantKarta[self.openStr][@"MIN"][@"Expression"];
    
    return YES;
	
}

- (BOOL)symbolAtIndexIsShowed:(NSUInteger)index
{
	
	return [self.openedIndexes containsObject:@(index)];
	
}

#pragma mark - Debug Methods

- (NSString *)description
{

	return [NSString stringWithFormat: @"numberOfCloseDigit: %d\n"
										"numberOfOpenDigit: %d\n"
										"summMin: %d\n"
										"summMax: %d\n"
										"stringOfExpression: %@\n"
										"stringMax: %@\n"
										"stringMin: %@\n"
										"openedIndexes: %@\n"
										"startOpenedIndexes: %@\n",
										self.numberOfCloseDigit,
										self.numberOfOpenDigit,
										self.summMin,
										self.summMax,
										self.stringOfExpression,
										self.stringMax,
										self.stringMin,
										self.openedIndexes,
										self.startOpenedIndexes];

}

@end
