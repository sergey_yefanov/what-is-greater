//
//  Calculator.h
//  Casino
//
//  Created by logo on 10/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ProcesingStrings.h"
#import "Expression.h"
#import "PartOfExpression.h"

@class Expression;

@interface Calculator : NSObject{

}
+(NSInteger) calculate:(NSString*) string;
@end
