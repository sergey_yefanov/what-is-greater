//
//  ESMGameProcess.m
//  Casino
//
//  Created by Sergey Yefanov on 04.08.14.
//  Copyright (c) 2014 Sergey Yefanov. All rights reserved.
//

#import "ESMGameProcess.h"
#import "ESMExpressionManager.h"
#import "ESMSaveManager.h"

NSString * const ESM_NEW_COUNT_COINS_IN_KOSHEL_NOTIFICATION			= @"ESM_NEW_COUNT_COINS_IN_KOSHEL_NOTIFICATION";
NSString * const ESM_NEW_COUNT_COINS_IN_KOSHEL_KEY					= @"ESM_NEW_COUNT_COINS_IN_KOSHEL_KEY";

NSString * const ESM_GAME_PROCESS_PERCENT_CHANGED_NOTIFICATION		= @"ESM_GAME_PROCESS_PERCENT_CHANGED_NOTIFICATION";
NSString * const ESM_GAME_PROCESS_KEY_PERCENT						= @"ESM_GAME_PROCESS_KEY_PERCENT";

NSString * const ESM_GAME_LEVEL_INDEX_CHANGED_NOTIFICATION			= @"ESM_GAME_LEVEL_INDEX_CHANGED_NOTIFICATION";
NSString * const ESM_GAME_LEVEL_INDEX_KEY							= @"ESM_GAME_LEVEL_INDEX_KEY";

NSString * const ESM_GAME_SIGN_PROCESSED_NOTIFICATION				= @"ESM_GAME_SIGN_PROCESSED_NOTIFICATION";
NSString * const ESM_GAME_SIGN_SELECTED_KEY							= @"ESM_GAME_SELECTED_SIGN_KEY";
NSString * const ESM_GAME_SIGN_OK_KEY								= @"ESM_GAME_OK_SIGN_KEY";
NSString * const ESM_GAME_DICT_STRINGS_KEY							= @"ESM_GAME_DICT_STRINGS_KEY";
NSString * const ESM_GAME_DICT_STRINGS_LEFT_KEY						= @"LeftPart";
NSString * const ESM_GAME_DICT_STRINGS_RIGHT_KEY					= @"RightPart";
NSString * const ESM_GAME_DICT_COINS_WIN_KEY						= @"ESM_GAME_DICT_COINS_WIN_KEY";

NSString * const ESM_GAME_EXPRESSON_LOADED_NOTIFICATION				= @"ESM_GAME_EXPRESSON_LOADED_NOTIFICATION";
NSString * const ESM_GAME_LOADED_LEVEL_INDEX_KEY					= @"ESM_GAME_LOADED_LEVEL_INDEX_KEY";
NSString * const ESM_GAME_LOADED_EXPRESSION_INDEX_KEY				= @"ESM_GAME_LOADED_EXPRESSION_INDEX_KEY";
NSString * const ESM_GAME_LOADED_EXPRESSION_LEFT_PART_KEY			= @"ESM_GAME_LOADED_EXPRESSION_LEFT_PART_KEY";
NSString * const ESM_GAME_LOADED_EXPRESSION_RIGHT_PART_KEY			= @"ESM_GAME_LOADED_EXPRESSION_RIGHT_PART_KEY";
NSString * const ESM_GAME_LOADED_EXPRESSION_NUMBER_CLOSE_DIGITS_KEY = @"ESM_GAME_LOADED_EXPRESSION_NUMBER_CLOSE_DIGITS_KEY";
NSString * const ESM_GAME_LOADED_COINS_IN_COSHEL					= @"ESM_GAME_LOADED_COINS_IN_COSHEL";
NSString * const ESM_GAME_LOADED_PERCENT							= @"ESM_GAME_LOADED_PERCENT";

@interface ESMGameProcess ()

@property (nonatomic) NSMutableArray *			showedExpressions;
@property (nonatomic) NSUInteger				maxProgress;
@property (nonatomic) NSUInteger				myProgress;
@property (nonatomic) ESMExpressionManager *	expressionManager;
@property (nonatomic) NSUInteger				expressionIndex;
@property (nonatomic) NSUInteger				coinsInKoshel;
@property (nonatomic) NSUInteger				maxGetCoins;

@end

@implementation ESMGameProcess

#pragma mark - Initialization

+ (instancetype)sharedInstance
{
	
	static id gameProcess = nil;
	static dispatch_once_t onceToken;
	
	dispatch_once(&onceToken, ^{
		
		gameProcess = [self new];
		
	});
	
    return gameProcess;
	
}

- (instancetype)init
{
	
	if (self = [super init]) {
		
		[self initialization];
		
	}
	
	return self;
	
}

- (void)initialization
{
	
	self.expressionManager = [ESMExpressionManager sharedInstance];
	
}

#pragma mark - setters

- (void)setLevelIndex:(NSUInteger)levelIndex
{
	
	if (self.gameState == ESMGameStateGame) {
		
		_levelIndex = levelIndex;
		[[NSNotificationCenter defaultCenter] postNotificationName:ESM_GAME_LEVEL_INDEX_CHANGED_NOTIFICATION
															object:[self class]
														  userInfo:@{ESM_GAME_LEVEL_INDEX_KEY : @(levelIndex)}];
		
	} else {
		
		@throw [NSException exceptionWithName:@"ERROR" reason:@"game state is not stop" userInfo:nil];
		
	}
	
}

- (void)setCoinsInKoshel:(NSUInteger)countCoinsInKoshel
{
	
	_coinsInKoshel = countCoinsInKoshel;
	
	[[NSNotificationCenter defaultCenter] postNotificationName:ESM_NEW_COUNT_COINS_IN_KOSHEL_NOTIFICATION
														object:[self class]
													  userInfo:@{ESM_NEW_COUNT_COINS_IN_KOSHEL_KEY : @(countCoinsInKoshel)}];
	
}

- (void)setPercentSuccess:(NSUInteger)percentSuccess
{
	
	if(percentSuccess > 100) {
		
		@throw [NSException exceptionWithName:@"Error"
									   reason:[NSString stringWithFormat:@"percent not correct: %lu", (unsigned long)percentSuccess]
									 userInfo:nil];
		
	}
	
	_percentSuccess = percentSuccess;
	
	[[NSNotificationCenter defaultCenter] postNotificationName:ESM_GAME_PROCESS_PERCENT_CHANGED_NOTIFICATION
														object:[self class]
													  userInfo:@{ESM_GAME_PROCESS_KEY_PERCENT : @(percentSuccess)}];
	
}

#pragma mark - Start / Stop game 

- (void)startGame
{
	self.gameState = ESMGameStateGame;
	NSDictionary *saveData = [[ESMSaveManager sharedInstance] loadSaveAtLevel:self.levelIndex];
	self.coinsInKoshel = [[ESMSaveManager sharedInstance] loadCoins];
	self.myProgress = ((NSNumber *)saveData[@"MyProgress"]).integerValue;
	self.maxProgress = ((NSNumber *)saveData[@"MaxProgress"]).integerValue;
	
	if (saveData[@"Opened"]) {
		
		self.showedExpressions = ((NSDictionary *)saveData[@"ShowedExpressions"]).mutableCopy;
		[self loadRandomExpressionInLevel:self.levelIndex];
		
	} else {
		
		NSLog(@"level closed");
		
	}
	
}

- (void)startTrainer
{
	
	[self loadRandomExpression];
	
}

- (void)stopGame
{
	
	self.gameState = ESMGameStateStop;
	
}

#pragma mark - Load Expression

- (BOOL)loadExpressionWithLevel:(NSUInteger)level atIndex:(NSUInteger)index;
{
	
    NSDictionary *dataExpression = [[ESMSaveManager sharedInstance] loadExpressionWithLevelIndex:level
																			 andExpressionIndex:index];
	
	if (dataExpression) {
		
		self.expressionManager.data = dataExpression;
		
		NSDictionary *leftDict = @{ @"Expression" : dataExpression[@"LeftPart"][@"Expression"],
								    @"Opened" : dataExpression[@"LeftPart"][@"Opened"]};
		
		NSDictionary *rightDict = @{ @"Expression" : dataExpression[@"RightPart"][@"Expression"],
									 @"Opened" : dataExpression[@"RightPart"][@"Opened"]};
		
		self.maxGetCoins = ((NSNumber *)dataExpression[@"Record"]).integerValue;
        
        NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                       @(level + 1), @"Level",
                                       @(index + 1), @"Expression",
                                       nil];
        
        [Flurry logEvent:@"Expression_Presented" withParameters:articleParams];
		
		[[NSNotificationCenter defaultCenter] postNotificationName:ESM_GAME_EXPRESSON_LOADED_NOTIFICATION
															object:[self class]
														  userInfo:@{
													 ESM_GAME_LOADED_LEVEL_INDEX_KEY : @(self.levelIndex),
													 ESM_GAME_LOADED_EXPRESSION_INDEX_KEY : @(self.expressionIndex),
													 ESM_GAME_LOADED_EXPRESSION_LEFT_PART_KEY : leftDict,
													 ESM_GAME_LOADED_EXPRESSION_RIGHT_PART_KEY : rightDict,
													 ESM_GAME_LOADED_EXPRESSION_NUMBER_CLOSE_DIGITS_KEY : @(self.expressionManager.numberCloseDigit),
													 ESM_GAME_LOADED_COINS_IN_COSHEL : @(self.coinsInKoshel),
													 ESM_GAME_LOADED_PERCENT : @([self calculatePercent])}];
		
		return YES;
		
	}

    return NO;
	
}

- (BOOL)loadNextExpression
{
	
	self.expressionIndex++;
	return [self loadExpressionWithLevel:self.levelIndex atIndex: self.expressionIndex];
	
}

- (BOOL)loadRandomExpression
{

	srand(time(NULL));
	NSInteger randomValue = rand() % [[ESMSaveManager sharedInstance] countLevels];
	return [self loadRandomExpressionInLevel:randomValue];
	
}

- (BOOL)loadRandomExpressionInLevel:(NSUInteger)level
{
	
	__block BOOL allShowed = YES;
	__block NSArray *notShowedArr = [NSArray new];
	
	[self.showedExpressions enumerateObjectsUsingBlock:^(NSNumber *obj, NSUInteger idx, BOOL *stop) {
		
		allShowed = obj.boolValue;
		
		if (!obj.boolValue) {
			
			notShowedArr = [notShowedArr arrayByAddingObject:@(idx)];
			
		}
		
	}];
	
	NSInteger randomValue;
	
	if(allShowed) {

		for(NSInteger i = 0; i < self.showedExpressions.count; i++) {
			
			self.showedExpressions[i] = @NO;
			
		}
		
		randomValue = arc4random() % [[ESMSaveManager sharedInstance] countExpressionsInLevel:level];

	} else {
		
		randomValue = ((NSNumber *)notShowedArr[rand() % notShowedArr.count]).integerValue;
		
	}
	
	NSLog(@"level: %ld expression: %ld", (unsigned long)level, (long)randomValue);
	self.expressionIndex = randomValue;
	
	return [self loadExpressionWithLevel:level atIndex: randomValue];
	
}

#pragma mark - Saver


- (void)saveProgress
{
	
	NSMutableDictionary *saverThisLevel = [[ESMSaveManager sharedInstance] loadSaveAtLevel:self.levelIndex].mutableCopy;
	
    [saverThisLevel setObject:self.showedExpressions forKey:@"ShowedExpressions"];
    [saverThisLevel setObject:@(self.maxProgress) forKey:@"MaxProgress"];
	[saverThisLevel setObject:@(self.myProgress) forKey:@"MyProgress"];
	
	[[ESMSaveManager sharedInstance] saveCoins:self.coinsInKoshel];
	[[ESMSaveManager sharedInstance] saveLevel:saverThisLevel atIndex:self.levelIndex];
	
}

#pragma mark - Sign process

- (void)didSelectSign:(ESMGameSelectedSign)selectedSign
{
	
	BOOL isCorrect = NO;
	NSDictionary *dictStrings;
	
	switch (selectedSign) {
			
		case ESMGameSelectedSignLess:
			
				if ([self.expressionManager leftPartIsLess]) {
					
					isCorrect = YES;
					dictStrings = [self.expressionManager stringsWithMorePart:@"right"];
					
				} else {
					
					isCorrect = NO;
					dictStrings = [self.expressionManager stringsWithMorePart:@"left"];
					
				}
			
			break;
			
		case ESMGameSelectedSignMore:
			
				if ([self.expressionManager leftPartIsMore]) {
					
					isCorrect = YES;
					dictStrings = [self.expressionManager stringsWithMorePart:@"left"];
					
				} else {
					
					isCorrect = NO;
					dictStrings = [self.expressionManager stringsWithMorePart:@"right"];
					
				}
			
			break;
			
		default:
			break;
	}
    
    if (isCorrect) {

        [Flurry logEvent:@"Sign_Correct"];
        
    } else {
        
        [Flurry logEvent:@"Sign_Fail"];
        
    }
	
	if (self.gameState == ESMGameStateGame) {
	
		if (isCorrect) {
			
			self.coinsInKoshel += self.expressionManager.numberCloseDigit + 1;
			self.myProgress += self.expressionManager.numberCloseDigit + 1;
			self.showedExpressions[self.expressionIndex] = @YES;
			
		}
		
		self.maxProgress += self.maxGetCoins;
		
		self.percentSuccess = [self calculatePercent];
		
		[self saveProgress];
		
	} else {
		
		[[ESMSaveManager sharedInstance] trainerIncrement];
		
	}
	
	[[NSNotificationCenter defaultCenter] postNotificationName:ESM_GAME_SIGN_PROCESSED_NOTIFICATION
														object:[self class]
													  userInfo:@{ESM_GAME_SIGN_SELECTED_KEY : @(selectedSign),
																 ESM_GAME_SIGN_OK_KEY : @(isCorrect),
																 ESM_GAME_DICT_STRINGS_KEY : dictStrings,
																 ESM_GAME_DICT_COINS_WIN_KEY : @(self.expressionManager.numberCloseDigit + 1)}];
	
}

- (NSInteger)calculatePercent
{
	
	if(!self.maxProgress) {
		
        return 0;
		
	}

    return (float)self.myProgress / (float)self.maxProgress * 100;
	
}

#pragma mark - Open digits

- (BOOL)openSymbolAtIndex:(NSUInteger)index
{
	
   return [self.expressionManager openSymbolAtIndex:index];
	
}

@end
