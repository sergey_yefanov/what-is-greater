/*
 *  ESMGameProcess.h
 *  Casino
 *
 *  Created by Sergey Yefanov on 04.08.14.
 *  Copyright (c) 2014 Sergey Yefanov. All rights reserved.
 */

#import <Foundation/Foundation.h>

extern NSString * const ESM_NEW_COUNT_COINS_IN_KOSHEL_NOTIFICATION;
extern NSString * const ESM_NEW_COUNT_COINS_IN_KOSHEL_KEY;

extern NSString * const ESM_GAME_PROCESS_PERCENT_CHANGED_NOTIFICATION;
extern NSString * const ESM_GAME_PROCESS_KEY_PERCENT;

extern NSString * const ESM_GAME_LEVEL_INDEX_CHANGED_NOTIFICATION;
extern NSString * const ESM_GAME_LEVEL_INDEX_KEY;

extern NSString * const ESM_GAME_SIGN_PROCESSED_NOTIFICATION;
extern NSString * const ESM_GAME_SIGN_SELECTED_KEY;
extern NSString * const ESM_GAME_SIGN_OK_KEY;
extern NSString * const ESM_GAME_DICT_STRINGS_KEY;
extern NSString * const ESM_GAME_DICT_STRINGS_LEFT_KEY;
extern NSString * const ESM_GAME_DICT_STRINGS_RIGHT_KEY;
extern NSString * const ESM_GAME_DICT_COINS_WIN_KEY;

extern NSString * const ESM_GAME_EXPRESSON_LOADED_NOTIFICATION;
extern NSString * const ESM_GAME_LOADED_LEVEL_INDEX_KEY;
extern NSString * const ESM_GAME_LOADED_EXPRESSION_INDEX_KEY;
extern NSString * const ESM_GAME_LOADED_EXPRESSION_LEFT_KEY;
extern NSString * const ESM_GAME_LOADED_EXPRESSION_OPENED_LEFT_KEY;
extern NSString * const ESM_GAME_LOADED_EXPRESSION_RIGHT_KEY;
extern NSString * const ESM_GAME_LOADED_EXPRESSION_OPENED_RIGHT_KEY;
extern NSString * const ESM_GAME_LOADED_EXPRESSION_NUMBER_CLOSE_DIGITS_KEY;
extern NSString * const ESM_GAME_LOADED_COINS_IN_COSHEL;
extern NSString * const ESM_GAME_LOADED_PERCENT;

typedef NS_ENUM (NSUInteger, ESMGameState) {
	
	ESMGameStateGame,
	ESMGameStateTrainer,
	ESMGameStateStop
	
};

typedef NS_ENUM (NSUInteger, ESMGameSelectedSign) {
	
	ESMGameSelectedSignMore,
	ESMGameSelectedSignLess
	
};

@interface ESMGameProcess : NSObject

/*
 *	Information about this game
 */

@property (nonatomic, readonly) NSUInteger		expressionIndex;
@property (nonatomic)			NSUInteger		levelIndex;

/*
 *	Information about coins
 */

@property (nonatomic, readonly) NSUInteger		coinsCount;
@property (nonatomic, readonly) NSUInteger		coinsInKoshel;
@property (nonatomic, readonly) NSUInteger		maxGetCoins;

/*
 *	Information about success
 */

@property (nonatomic)			ESMGameState	gameState;
@property (nonatomic, readonly) NSUInteger		percentSuccess;
@property (nonatomic, readonly) NSUInteger		winCoins;
@property (nonatomic, readonly) BOOL			nextLevelOpened;

/*
 *	Information about success
 */

@property (nonatomic, readonly) NSArray *		recordCombinationIndexes;

+ (instancetype)sharedInstance;

- (BOOL)openSymbolAtIndex:(NSUInteger)index;
- (void)didSelectSign:(ESMGameSelectedSign) selectedSign;

/*
 *	start/stop game
 */

- (void)startGame;
- (void)startTrainer;
- (void)stopGame;

/*
 *	Load expressions
 */

- (BOOL)loadExpressionWithLevel:(NSUInteger)level atIndex:(NSUInteger)index;
- (BOOL)loadNextExpression;
- (BOOL)loadRandomExpression;
- (BOOL)loadRandomExpressionInLevel:(NSUInteger)level;

@end
