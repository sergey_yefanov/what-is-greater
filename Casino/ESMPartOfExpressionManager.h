//
//  PartOfExpression.h
//  Casino
//
//  Created by Sergey Yefanov on 04.08.14.
//  Copyright (c) 2014 Sergey Yefanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ESMPartOfExpressionManager : NSObject

@property (nonatomic)			NSDictionary *		data;
@property (nonatomic, readonly) NSUInteger			numberOfCloseDigit;
@property (nonatomic, readonly) NSUInteger			numberOfOpenDigit;
@property (nonatomic, readonly) NSInteger			summMin;
@property (nonatomic, readonly) NSInteger			summMax;
@property (nonatomic, readonly) NSString *			stringOfExpression;
@property (nonatomic, readonly) NSString *			stringMax;
@property (nonatomic, readonly) NSString *			stringMin;
@property (nonatomic, readonly) NSArray *			openedIndexes;
@property (nonatomic, readonly) NSArray *			startOpenedIndexes;

- (instancetype)initWithData:(NSDictionary *)data;
- (BOOL)openDigitAtIndex:(NSUInteger)index;

@end
