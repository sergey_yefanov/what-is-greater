//
//  ESMButtonNextExpression.m
//  Casino
//
//  Created by Sergey Yefanov on 04.08.14.
//  Copyright (c) 2014 Sergey Yefanov. All rights reserved.
//

static CGFloat	  const ANIMATION_DURATION	= 0.5;
static NSString * const FILE_NAME			= @"Images.plist";
static NSString * const KEY_NAME			= @"NextCursor";

#import "ESMFileManager.h"
#import "ESMButtonNextExpression.h"

@implementation ESMButtonNextExpression

#pragma mark - Initialization

- (instancetype)init
{
	
	if(self = [super init]) {

		[self initialize];
		
	}
	
	return self;
	
}

- (instancetype)initWithFrame:(CGRect)frame
{
	
	if (self = [super initWithFrame:frame]) {
		
		[self initialize];
		
	}
	
	return self;
	
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
	
	if (self = [super initWithCoder:aDecoder]) {
		
		[self initialize];
		
	}
	
	return self;
	
}

- (void)initialize
{
	
	self.animationImages = self.animationArr;
    self.animationDuration = ANIMATION_DURATION;
	self.animationRepeatCount = 5;
	self.contentMode = UIViewContentModeCenter;
	
	self.image = self.animationArr[0];
	self.userInteractionEnabled = YES;
	
}

#pragma mark - Getters

- (NSArray *)animationArr
{
	static NSArray *imagesArr;
	static dispatch_once_t onceToken;
	
	dispatch_once(&onceToken, ^{
		
		NSArray *imagesPaths = [[ESMFileManager sharedInstance] objectWithName:FILE_NAME][KEY_NAME];
		imagesArr = [NSArray array];
		
		for (NSString *imagePath in imagesPaths) {
			
			imagesArr = [imagesArr arrayByAddingObject: [UIImage imageNamed:imagePath]];
			
		}
		
	});

	return imagesArr;
    
}

#pragma mark - Controls

- (void)show
{
	
	[self startAnimating];
	
	[UIView animateWithDuration:ANIMATION_DURATION
						  delay:0.0f
						options:UIViewAnimationOptionCurveLinear
					 animations:^{
						 
						 self.alpha = 1.0f;
						 
					 }
	 
					 completion:nil];
	
}

- (void)hide
{
	
	[self stopAnimating];
	self.alpha = 0.0f;
	
}

#pragma mark - Delegate methods

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	
    if([self.delegate respondsToSelector:@selector(didTouchNextExpression)]) {
		
        [self.delegate didTouchNextExpression];
		
	} else {
		
        NSLog(@"error delegate didTouchNextExpression");
		
	}
	
}

@end