//
//  ESMFinalVC.m
//  Casino
//
//  Created by Sergey Yefanov on 04.08.14.
//  Copyright (c) 2014 Sergey Yefanov. All rights reserved.
//

#import "ESMFinalVC.h"
#import "ESMGameProcess.h"

@interface ESMFinalVC ()

@property (nonatomic, weak) IBOutlet UILabel *		labelScore;
@property (nonatomic, weak) IBOutlet UILabel *		labelReset;
@property (nonatomic, weak) IBOutlet UIImageView *	reset;

@end

@implementation ESMFinalVC

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{

    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
		
        self.reset.userInteractionEnabled = YES;
		
    }
	
    return self;
	
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	
	UITouch *touch = [[event allTouches] anyObject];
	
	if ([touch view] == self.reset || [touch view] == self.labelReset) {
		
        [self resetGame];
		
	}
	
}

- (void)resetGame
{
	
    //[parent resetGame];
    [self.navigationController popToRootViewControllerAnimated:YES];
	
}

@end
