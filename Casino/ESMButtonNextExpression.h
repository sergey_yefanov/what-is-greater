//
//  ButtonNext.h
//  Casino
//
//  Created by Sergey Yefanov on 04.08.14.
//  Copyright (c) 2014 Sergey Yefanov. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ButtonNextExpressonDelegate;

@interface ESMButtonNextExpression : UIImageView

@property (nonatomic, weak) id <ButtonNextExpressonDelegate> delegate;

- (void)show;
- (void)hide;

@end

@protocol ButtonNextExpressonDelegate <NSObject>

- (void)didTouchNextExpression;

@end
