//
//  ESMExpressionManager.h
//  Casino
//
//  Created by Sergey Yefanov on 04.08.14.
//  Copyright (c) 2014 Sergey Yefanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ESMExpressionManager : NSObject

@property (nonatomic, readonly) NSUInteger		numberCloseDigit;
@property (nonatomic, readonly) BOOL			leftPartIsLess;
@property (nonatomic, readonly) BOOL			leftPartIsMore;
@property (nonatomic, readonly) BOOL			rightPartIsLess;
@property (nonatomic, readonly) BOOL			rightPartIsMore;
@property (nonatomic)			NSDictionary *	data;

+ (instancetype)sharedInstance;

- (BOOL)openSymbolAtIndex:(NSUInteger)index;
- (NSDictionary *)stringsWithMorePart:(NSString *)morePart;

@end
