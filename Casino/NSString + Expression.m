//
//  Strings.m
//  Casino
//
//  Created by Sergey Yefanov on 04.08.14.
//  Copyright (c) 2014 Sergey Yefanov. All rights reserved.
//

#import "NSString + Expression.h"

@implementation NSString (Expression)

#pragma mark - Parce String

- (NSUInteger)binUInteger
{
	
    int digit = 0;
    
    NSRange symbolRange;
    symbolRange.length = 1;
    
    for (int i = 0; i < self.length; i++) {
        
        symbolRange.location = i;
        NSString *var = [self substringWithRange:symbolRange];
        digit = digit << 1 | var.boolValue;
		
    }
	
    return digit;
	
}

- (NSArray *)binArray
{
	
	NSArray *binArr = [NSArray new];
	
    NSRange symbolRange;
    symbolRange.length = 1;
    
    for (NSUInteger i = 0; i < self.length; i++) {
        
        symbolRange.location = i;
        NSString *var = [self substringWithRange:symbolRange];
		binArr = [binArr arrayByAddingObject: @([var binBool])];
		
	}
	
	return binArr;
}

- (NSArray *)trueIndexesArray
{
	
	NSArray *binArr = [NSArray new];
	
    NSRange symbolRange;
    symbolRange.length = 1;
    
    for (int i = 0; i < self.length; i++) {
        
        symbolRange.location = i;
        NSString *var = [self substringWithRange:symbolRange];
		
		if ([var binBool]) {
			
			binArr = [binArr arrayByAddingObject:@(i)];
			
		}
	}
	
	return binArr;
	
}

- (BOOL)binBool
{
	
	return self.boolValue;
	
}

#pragma mark - /// - ///

+ (NSString *)stringWithBinDigit:(NSUInteger)digit andNumberSymbol:(NSInteger)numberSymbol
{
    
    NSMutableString *str = [NSMutableString stringWithString:@""];
    
    if (!digit) {
		
        NSString *strNull = [NSString stringWithFormat:@"%d", 0];
        return strNull;
		
    }
    
    for (NSInteger i = numberSymbol - 1; i >= 0; i--) {
		
        NSInteger a = (digit >> i) & 1;
        NSString *subStr = [NSString stringWithFormat:@"%ld", (long)a];
        [str insertString:subStr atIndex:(str.length)];
		
    }
    
    return str.copy;
}

- (BOOL)binAtIndex:(NSUInteger)index
{
    
    if(self.length <= index) {
		
        return NO;
		
	}
    
    return [[self symbolAtIndex:index] binBool];
}

- (NSString*)symbolAtIndex:(NSUInteger)index
{
    
    if(self.length <= index) {
		
        return nil;
		
	}
    
    NSRange rangeSymb;
    rangeSymb.length = 1;
    rangeSymb.location = index;
    
    return [self substringWithRange:rangeSymb];
	
}

- (NSUInteger)numberOfBracket
{
	
    NSInteger number = 0;
    NSString* subString;
    NSRange range;
    int i = 0;
    range.length = 1;
    
    do {
        range.location = i;
        subString = [self substringWithRange:range];
        
        if([subString isBracket])
            number++;
        
        i++;
        
    } while (i < self.length);
    
    return number;
}

- (NSUInteger)numberOfDigit
{
	
    NSInteger number = 0;
    NSString* subString;
    NSRange range;
    int i = 0;
    range.length = 1;

    do {
		
        range.location = i;
        subString = [self substringWithRange:range];
        
        if ([subString isDigit]) {
			
            number++;
        
		}
		
        i++;
        
    } while (i < self.length);

    return number;
	
}

- (NSUInteger)findDigitAtIndex:(NSUInteger)digit
{
	
    NSInteger count = 0;
    
    NSString* subString;
    NSRange range;
    int i = 0;
    range.length = 1;
    
    do {
		
        range.location = i;
        subString = [self substringWithRange:range];
        
        if([subString isDigit]) {
			
            count++;
			
		}
        
        i++;
		
    } while (count <= digit);
    
    return range.location;
}

- (NSUInteger)numberOfSign {
	
    NSInteger number = 0;
    NSString* subString;
    NSRange range;
    int i = 0;
    range.length = 1;

    do{
        range.location = i;
        subString = [self substringWithRange:range];
        
        if([subString isSign])
            number++;
        
        i++;
        
    }while(i < self.length);
    
    return number;
}

- (BOOL) isOpenBracket{
	
    return [self compare: @"("] == NSOrderedSame;
	
}

-(BOOL) isCloseBracket{
	
    return [self compare: @")"] == NSOrderedSame;
	
}

- (BOOL)isBracket {
	
    if([self isOpenBracket])
        return YES;
    
    if([self isCloseBracket])
        return YES;
    
    return NO;
	
}

- (BOOL)isPlus {
    return [self compare: @"+"] == NSOrderedSame? YES : NO;
}

- (BOOL)isMinus {
    return [self compare: @"-"] == NSOrderedSame? YES : NO;
}

- (BOOL)isMultiply {
    return [self compare: @"*"] == NSOrderedSame? YES : NO;
}

- (BOOL)isDivision {
    return [self compare: @"/"] == NSOrderedSame? YES : NO;
}

- (BOOL)isSign {
	
    if([self isMinus])
        return YES;
    
    if([self isPlus])
        return YES;
    
    if([self isDivision])
        return YES;
    
    if([self isMultiply])
        return YES;
    
    return NO;
	
}

- (BOOL)isDigit{
    
    if([self compare: @"0"] == NSOrderedSame)
        return YES;
    
    if([self compare: @"1"] == NSOrderedSame)
        return YES;
    
    if([self compare: @"2"] == NSOrderedSame)
        return YES;
    
    if([self compare: @"3"] == NSOrderedSame)
        return YES;
    
    if([self compare: @"4"] == NSOrderedSame)
        return YES;
    
    if([self compare: @"5"] == NSOrderedSame)
        return YES;
    
    if([self compare: @"6"] == NSOrderedSame)
        return YES;
    
    if([self compare: @"7"] == NSOrderedSame)
        return YES;
    
    if([self compare: @"8"] == NSOrderedSame)
        return YES;
    
    if([self compare: @"9"] == NSOrderedSame)
        return YES;
    
    return NO;
}


- (NSString *)cutLeftPart {
    NSRange range = [self rangeOfString:@" "];
    return [self substringToIndex:range.location];
}

- (NSString *)cutRightPart
{
	
    NSRange range = [self rangeOfString:@" "];
    return [self substringFromIndex:range.location + range.length];
	
}

- (NSString *)insertBracket
{
    
    NSRange findRange;
    findRange.location = 0;
    findRange.length = self.length;
    NSMutableString* tempString = self.mutableCopy;
    NSString* str;
    NSRange rangeOfSign = [tempString rangeOfString:@"*" options:NSLiteralSearch range:findRange];
    NSRange rangeOfPart;
    
    while(rangeOfSign.length != 0){
        
        rangeOfPart.location = rangeOfSign.location - 1;
        rangeOfPart.length = 1;
        
        if([[tempString substringWithRange:rangeOfPart] compare: @")"] == NSOrderedSame){
            str = [tempString substringToIndex:rangeOfSign.location];

            NSRange termA = [str findTermWithEnd];
            [tempString insertString:@"(" atIndex:termA.location];
        }
        else {
            if([[tempString substringWithRange:rangeOfPart] isDigit]){
                NSRange digitA = [[tempString substringToIndex:rangeOfSign.location] findDigitWithEnd];
                [tempString insertString:@"(" atIndex:digitA.location];
            }
        }
        
        rangeOfPart.location = rangeOfSign.location + 2;
        rangeOfPart.length = 1;
        
        if([[tempString substringWithRange:rangeOfPart] compare: @"("] == NSOrderedSame){
            NSRange termB = [[tempString substringFromIndex:rangeOfPart.location] findTermWithStart];
            [tempString insertString:@")" atIndex:rangeOfPart.location + termB.location + 1];
        }
        else {
            if([[tempString substringWithRange:rangeOfPart] isDigit]){
                NSRange digitB = [[tempString substringFromIndex:rangeOfPart.location] findDigitWithStart];
                [tempString insertString:@")" atIndex:rangeOfPart.location + digitB.location + 1];
            }
        }
        
        findRange.location = rangeOfSign.location + 2;
        findRange.length = tempString.length - findRange.location;
        rangeOfSign = [tempString rangeOfString:@"*" options:NSLiteralSearch range:findRange];
		
    }
    
    return tempString.copy;
}


- (NSString *)cutDigit {
    NSRange range = [self findDigitWithEnd];
    return [self substringWithRange:range];
}

- (NSRange) findDigitWithEnd{
    
    //выделяет число, начиная с конца
    
    NSString* subString;
    NSRange range;
    int i = self.length - 1;
    range.length = 1;
    //int a = string.length;
    do{
        range.location = i;
        subString = [self substringWithRange:range];
        i--;
        
    }while([subString isDigit] && i >= 0);
    
    if(![subString isDigit])
        i++;
    
    range.location = i + 1;
    
    range.length = self.length - range.location;
    return range;
}

- (NSRange) findDigitWithStart{
    NSString* subString;
    NSRange range;
    int i = 0;
    range.length = 1;
    //int a = string.length;
    do{
        range.location = i;
        subString = [self substringWithRange:range];
        i++;
        
    }while([subString isDigit] && i < self.length);
    
    if(![subString isDigit])
        i--;
    
    range.location = i - 1;
    
    range.length = i;
    return range;
}

- (NSString*) cutTerm{
    NSRange range = [self findTermWithEnd];
   return [self substringWithRange:range];
}

- (NSRange) findTermWithEnd{
    int count = 0;
    int i = self.length - 1;
    NSRange range;
    NSString *subString;
    range.length = 1;
    range.location = i;
    
    do{ 
        range.location = i;
        subString = [self substringWithRange:range];
        
        if([subString compare:@"("] == NSOrderedSame)
            count--;
        
        if([subString compare:@")"] == NSOrderedSame)
            count++;
        
        i--;
        
    }while (count != 0 && i >= 0);
    
    if(i != 0)
        i++;
    
    range.location = i + 1;
    
    range.length = self.length - range.location - 1;
    return range;
}

- (NSRange)findTermWithStart{
    int count = 0;
    int i = 0;
    NSRange range;
    NSString *subString;
    range.length = 1;
    range.location = i;
    
    do{ 
        range.location = i;
        subString = [self substringWithRange:range];
        
        if([subString compare:@"("] == NSOrderedSame)
            count++;
        
        if([subString compare:@")"] == NSOrderedSame)
            count--;
        
        i++;
        
    }while (count != 0 && i < self.length);
    
    if(i != 0)
        i--;
    
    range.location = i - 1;
    
    range.length = i;
    return range;
}

@end
