//
//  ViewPartKartaSymbol.m
//  Casino
//
//  Created by Sergey Yefanov on 04.08.14.
//  Copyright (c) 2014 Sergey Yefanov. All rights reserved.
//

#import "ESMPartOfExpressionView.h"
#import "ESMSymbolView.h"
#import "NSString + Expression.h"

static NSUInteger const GAP_DIGIT		= 46;
static NSUInteger const GAP_SIGN		= 36;
static NSUInteger const GAP_BRACKET		= 46;
static CGFloat	  const MAX_FACTOR		= 1.5;

@interface ESMPartOfExpressionView () <ESMSymbolViewDelegate>

@property (nonatomic) NSArray *	massKartaSymbol;

@end

@implementation ESMPartOfExpressionView

#pragma mark - Initialization

- (instancetype)init
{
	
	if (self = [super init]) {
	
		[self initialize];
		
	}
	
	return self;
	
}

- (instancetype)initWithFrame:(CGRect)frame
{
	
	if (self = [super initWithFrame:frame]) {
		
		[self initialize];
		
	}
	
	return self;
	
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
	
	if (self = [super initWithCoder:aDecoder]) {
		
		[self initialize];
		
	}
	
	return self;
	
}

- (void)initialize
{
	
	self.backgroundColor = [UIColor clearColor];
	self.massKartaSymbol = [NSArray new];
	
}

- (void)initializeSubViews
{
	
	NSString *opened = self.data[@"Opened"];
	
	NSUInteger indexOfDigit = 0;
	
	NSUInteger offset = 0;
	
	for (NSInteger i = 0; i < self.expressionStr.length; i++) {
		
		NSString *symbol = [self.expressionStr symbolAtIndex:i];
		offset += [self offsetNextPositionOfSymbol:symbol];
		
	}
	
	NSUInteger x = (self.frame.size.width - offset) / 2;
	
	for (NSInteger i = 0; i < self.expressionStr.length; i++) {
		
		NSString *symbol = [self.expressionStr symbolAtIndex:i];
		BOOL isOpen = YES;
		ESMSymbolView *kartaSymbol;
		
		if (symbol.isDigit) {
			
			isOpen = [opened binAtIndex:indexOfDigit];
			indexOfDigit++;
			kartaSymbol = [[ESMSymbolView alloc] initWithFrame:CGRectMake(x, 30, 100, 100)];
            kartaSymbol.exclusiveTouch = YES;
			
		} else {
			
			kartaSymbol = [[ESMSymbolView alloc] initWithFrame:CGRectMake(x + 25, 55, 50, 50)];
            kartaSymbol.exclusiveTouch = YES;
			
		}
		
		kartaSymbol.symbol = symbol;
		kartaSymbol.open = isOpen;
		kartaSymbol.delegate = self;
		[self addSubview:kartaSymbol];
		
		self.massKartaSymbol = [self.massKartaSymbol arrayByAddingObject:kartaSymbol];
		
		x += [self offsetNextPositionOfSymbol:symbol];
		
	}

	
}

#pragma mark - Setters

- (void)setFrame:(CGRect)frame
{
	
	[super setFrame:frame];
	[self initializeSubViews];
	
}



- (void)setData:(NSDictionary *)data
{
	
	_data = data;
	self.expressionStr = data[@"Expression"];
		
}

- (void)setExpressionStr:(NSString *)expressionStr
{
	
	_expressionStr = expressionStr;
	
	[self.massKartaSymbol enumerateObjectsUsingBlock:^(ESMSymbolView *symbolView, NSUInteger idx, BOOL *stop) {
		
		symbolView.symbol =  [expressionStr symbolAtIndex:idx];
		
	}];
	
}

#pragma mark - Getters
    
- (CGFloat)factor
{
    
    NSInteger numberOfDigital = [self.expressionStr numberOfDigit];
    NSInteger numberOfSign = [self.expressionStr numberOfSign];
    NSInteger numberOfBracket = [self.expressionStr numberOfBracket];
    
    CGFloat factor = (float) self.frame.size.width / (numberOfSign * GAP_SIGN + numberOfDigital * GAP_DIGIT + numberOfBracket * GAP_BRACKET);

    return MIN(factor, MAX_FACTOR);
	
}

- (NSInteger)offsetNextPositionOfSymbol:(NSString *)symbol
{
    
    if (symbol.isDigit) {
		
        return GAP_DIGIT * self.factor;
		
	} else if (symbol.isSign) {
		
        return GAP_SIGN * self.factor;
		
	} else if (symbol.isBracket) {
		
        return GAP_BRACKET * self.factor;
		
	}
    
	@throw [NSException exceptionWithName:@"ERROR" reason:@"unktoun symbol" userInfo:nil];
	
}

#pragma makr - Symbol view delegate methods

- (void)symbolDidTouch:(ESMSymbolView *)symbolView
{
	
	if ([self.delegate respondsToSelector:@selector(partOfExpressionView:didTouchAtIndex:)]) {
		
		NSUInteger index = [self.massKartaSymbol indexOfObject:symbolView];
		[self.delegate partOfExpressionView:self didTouchAtIndex:index];
		
	} else {
		
		NSLog(@"delegate have not selector partOfExpressionView:didTouchAtIndex:");
		
	}
	
}

#pragma mark - Blink

- (void)blinkSymbolAtIndex:(NSUInteger)symbolIndex
{
	
	ESMSymbolView *kartaSymb = self.massKartaSymbol[symbolIndex];
	[kartaSymb blink];
	
}

- (void)blinkSymbolsAtIndexes:(NSArray *)symbolsIndexes
{
	
	for (NSNumber *index in symbolsIndexes) {
		
		[self blinkSymbolAtIndex:index.integerValue];
		
    }
	
}

- (void)blinkAllSymbols
{
	
	for (NSInteger i = 0; i < self.massKartaSymbol.count; i++) {
		
        [self blinkSymbolAtIndex:i];
		
    }
	
}

#pragma mark - Open

- (void)openSymbolAtIndex:(NSUInteger)symbolIndex
{
	
	ESMSymbolView *kartaSymb = self.massKartaSymbol[symbolIndex];
    [kartaSymb open];
	
}

- (void)openSymbolsAtIndexes:(NSArray *)symbolsIndexes
{
	
	for (NSNumber *index in symbolsIndexes) {
		
		[self openSymbolAtIndex:index.integerValue];
		
	}
	
}

- (void)openAllSymbols
{
	
	for (NSInteger i = 0; i < self.massKartaSymbol.count; i++) {
		
		[self openSymbolAtIndex:i];
		
	}
	
}

#pragma mark - Move karts

- (void)moveKartAtIndex:(NSUInteger)kartIndex
{
	
	ESMSymbolView *kartaSymb = self.massKartaSymbol[kartIndex];
	[kartaSymb move];
	
}

- (void)moveKartsAtIndexes:(NSArray *)kartIndexes
{
	
	for (NSNumber *index in kartIndexes) {
		
		[self moveKartAtIndex:index.integerValue];
		
	}
	
}

- (void)moveAllKarts
{
	
	for (NSInteger i = 0; i < self.massKartaSymbol.count; i++) {
		
        ESMSymbolView *kartaSymb = self.massKartaSymbol[i];
        [kartaSymb move];
		
    }
	
}

@end
