//
//  ESMPartOfExpressionView.h
//  Casino
//
//  Created by Sergey Yefanov on 04.08.14.
//  Copyright (c) 2014 Sergey Yefanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ESMPartOfExpressionDelegate;

@interface ESMPartOfExpressionView : UIView

@property (nonatomic)		NSDictionary *						data;
@property (nonatomic)		NSString *							expressionStr;
@property (nonatomic, weak) id <ESMPartOfExpressionDelegate>	delegate;

- (void)blinkSymbolAtIndex:(NSUInteger)symbolIndex;
- (void)blinkSymbolsAtIndexes:(NSArray *)symbolsIndexes;
- (void)blinkAllSymbols;

- (void)openSymbolAtIndex:(NSUInteger)symbolIndex;
- (void)openSymbolsAtIndexes:(NSArray *)symbolsIndexes;
- (void)openAllSymbols;

- (void)moveKartAtIndex:(NSUInteger)kartIndex;
- (void)moveKartsAtIndexes:(NSArray *)kartIndexes;
- (void)moveAllKarts;

@end

@protocol ESMPartOfExpressionDelegate <NSObject>

- (void)partOfExpressionView:(ESMPartOfExpressionView *)partOfExpressionView didTouchAtIndex:(NSUInteger)index;

@end
