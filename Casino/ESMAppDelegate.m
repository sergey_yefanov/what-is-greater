//
//  AppDelegate.m
//  Casino
//
//  Created by Sergey Yefanov on 04.08.14.
//  Copyright (c) 2014 Sergey Yefanov. All rights reserved.
//

#import "ESMAppDelegate.h"
#import "ESMButtonNextExpression.h"
#import "ESMKoshelBackView.h"
#import "ESMKoshelFrontView.h"
#import "ESMSymbolView.h"
#import "ESMExpressionView.h"

@implementation ESMAppDelegate

@synthesize window = _window;
@synthesize myNavigationController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
	[UIApplication sharedApplication].statusBarHidden = YES;
	[ESMSaveManager sharedInstance];
	[ESMButtonNextExpression new];
	[ESMKoshelBackView new];
	[ESMKoshelFrontView new];
	[ESMSymbolView new];
	[ESMExpressionView new];
    
    [Flurry setCrashReportingEnabled:YES];
	[Flurry setLogLevel:FlurryLogLevelAll];
	[Flurry startSession:@"7BXCWCWBWSJKGFBXS5NK"];

	
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [Flurry endTimedEvent:@"LOG_APPLICATION_RUNNED" withParameters:nil];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [Flurry logEvent:@"LOG_APPLICATION_RUNNED" timed:YES];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [Flurry endTimedEvent:@"LOG_APPLICATION_RUNNED" withParameters:nil];
}


@end
