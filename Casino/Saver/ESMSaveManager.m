//
//  Saver.m
//  Casino
//
//  Created by Sergey Yefanov on 04.08.14.
//  Copyright (c) 2014 Sergey Yefanov. All rights reserved.
//

#import "ESMFileManager.h"
#import "ESMSaveManager.h"

static NSString * const KEY_LEVELS	= @"Levels";

@interface ESMSaveManager ()

@property (nonatomic) NSUserDefaults *	userDefaults;
@property (nonatomic) NSInteger			countLevels;
@property (nonatomic) NSMutableArray *	saveLevels;

@end

@implementation ESMSaveManager

#pragma mark - Initialization

+ (instancetype)sharedInstance
{
	
	static id saveManager = nil;
	static dispatch_once_t onceToken;
	
	dispatch_once(&onceToken, ^{
		
		saveManager = [ESMSaveManager new];
		
	});

    return saveManager;
	
}

- (instancetype)init
{
	
    if(self = [super init]){
		
        self.userDefaults = [NSUserDefaults standardUserDefaults];
		
		self.countLevels = self.baseLevelsArr.count;
		
		self.saveLevels = [self.userDefaults objectForKey:KEY_LEVELS];
        
        if (!self.saveLevels) {
            
			[self clearSave];
			
        }
		
    }
	
    return self;
	
}

#pragma mark - Setters

- (void)saveCoins:(NSUInteger)coins
{
	
	[self.userDefaults setObject:@(coins) forKey:@"userCoins"];
	[self.userDefaults synchronize];
	
}

- (NSUInteger)loadCoins
{
	
	return ((NSNumber *)[self.userDefaults objectForKey:@"userCoins"]).integerValue;
	
}

#pragma mark - Getters

- (NSArray *)baseLevelsArr
{
	
	static NSArray *levelsArr;
	static dispatch_once_t onceToken;
	
	dispatch_once(&onceToken, ^{
		
		levelsArr = [[ESMFileManager sharedInstance] objectWithName:@"Levels.plist"];
		
	});
	
	return levelsArr;
	
}

#pragma mark - Load Levels

- (NSDictionary *)loadExpressionWithLevelIndex:(NSUInteger)levelIndex andExpressionIndex:(NSUInteger)expressionIndex
{
	
	if (levelIndex >= self.countLevels) {
		
        return nil;
		
	}
    
    NSArray* arrLevel = self.baseLevelsArr[levelIndex];
    
    if(expressionIndex >= arrLevel.count) {
		
        return nil;
		
	}
    
  return arrLevel[expressionIndex];
	
}

#pragma mark - Saver

- (NSDictionary *)loadSaveAtLevel:(NSUInteger)levelIndex
{
    
    if (levelIndex >= self.saveLevels.count) {
		
        return nil;
		
	}
    
    return [self.saveLevels objectAtIndex:levelIndex];
	
}

- (void)saveLevel:(NSDictionary *)dict atIndex:(NSUInteger)index
{
	
    [self.saveLevels replaceObjectAtIndex:index withObject:dict];
    [self.userDefaults setObject:self.saveLevels forKey:KEY_LEVELS];
    [self.userDefaults synchronize];
	
}

- (void)clearSave
{
	
	self.saveLevels = [NSMutableArray new];
	
	for (NSInteger i = 0; i < self.countLevels; i++) {
		
		@autoreleasepool {

			NSMutableDictionary *thisLevelDict = [NSMutableDictionary new];
			NSMutableArray *showedExpressionsArr = [NSMutableArray new];
			
			for (NSUInteger j = 0; j < [self countExpressionsInLevel:i]; j++) {
				
				[showedExpressionsArr addObject:@NO];
				
			}
			
			[thisLevelDict setObject:showedExpressionsArr forKey:@"ShowedExpressions"];
			[thisLevelDict setObject:@(i * 5 + 5) forKey:@"Cost"];
			[thisLevelDict setObject:@(0) forKey:@"MaxProgress"];
			[thisLevelDict setObject:@(0) forKey:@"MyProgress"];
			[self.saveLevels addObject:thisLevelDict];
			
		}
		
	}
	
	[self.saveLevels[0] setObject:@YES forKey:@"Opened"];
	[self.userDefaults setObject:@(0) forKey:@"TrainerCount"];
	[self.userDefaults setObject:self.saveLevels forKey:KEY_LEVELS];
	[self.userDefaults setObject:@(0) forKey:@"userCoins"];
}

#pragma mark - Count

- (NSInteger)countOpened
{
	
	NSInteger countOpened = 0;
	
	for(NSInteger i = 0; i < [self countLevels]; i++) {
		
		NSDictionary *saveData = [[ESMSaveManager sharedInstance] loadSaveAtLevel:i];
		
		if (saveData[@"Opened"]) {
			
			countOpened++;
			
		}
		
	}
	
	return countOpened;
	
}

- (void)trainerIncrement
{
	
	NSUInteger trainerCount = ((NSNumber *)[self.userDefaults objectForKey:@"TrainerCount"]).integerValue;
	[self.userDefaults setObject:@(trainerCount + 1) forKey:@"TrainerCount"];
	
}

- (NSUInteger)trainerCount
{
	
	return 	((NSNumber *)[self.userDefaults objectForKey:@"TrainerCount"]).integerValue;
	
}

- (BOOL)isFull
{
	
	return ((NSNumber *)[self.userDefaults objectForKey:@"FullVersion"]).boolValue;
	
}

- (void)fullOk
{
	
	[self.userDefaults setObject:@YES forKey:@"FullVersion"];
	
}

- (NSUInteger)countExpressionsInLevel:(NSUInteger)levelIndex
{
    
    if (levelIndex >= self.countLevels) {
		
        return 0;
		
	}
    
    NSArray* level = self.baseLevelsArr[levelIndex];
    return level.count;
	
}

- (NSString *)nameLevelAtIndex:(NSInteger)index
{
	
	NSString *name;
	
	switch (index) {
			
		case 0:
			name = NSLocalizedString(@"Однозначные числа", nil);
			break;
			
		case 1:
			name = NSLocalizedString(@"Двузначные числа", nil);
			break;
			
		case 2:
			name = NSLocalizedString(@"Сложение однозначных", nil);
			break;
			
		case 3:
			name = NSLocalizedString(@"Сложение однозначных", nil);
			break;
			
		case 4:
			name = NSLocalizedString(@"Вычитание однозначных", nil);
			break;
			
		case 5:
			name = NSLocalizedString(@"Сложение однозначных", nil);
			break;
			
		case 6:
			name = NSLocalizedString(@"Вычитание однозначных", nil);
			break;
			
		case 7:
			name = NSLocalizedString(@"Сложение и вычитание однозначных", nil);
			break;
			
		case 8:
			name = NSLocalizedString(@"Сложение однозначных и двузначные", nil);
			break;
			
		case 9:
			name = NSLocalizedString(@"Деление двузначных на однозначное", nil);
			break;
			
		case 10:
			name = NSLocalizedString(@"Умножение двузначных на однозначное", nil);
			break;
			
		case 11:
			name = NSLocalizedString(@"Самое сложное", nil);
			break;
			
		default:
			name = @"";
			break;
	}
	
	return name;
	
}

- (void)openLevelAtIndex:(NSUInteger)levelIndex
{
	
	NSMutableDictionary *save = ([self loadSaveAtLevel:levelIndex]).mutableCopy;
	
	[save setObject:@YES forKey:@"Opened"];
	
	[self saveLevel:save atIndex:levelIndex];
	
}

@end
