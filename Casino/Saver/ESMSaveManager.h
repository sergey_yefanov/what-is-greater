//
//  Saver.h
//  Casino
//
//  Created by Sergey Yefanov on 04.08.14.
//  Copyright (c) 2014 Sergey Yefanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ESMSaveManager : NSObject

@property (nonatomic, readonly) NSInteger countLevels;

+ (instancetype)sharedInstance;

/*
 *	Load levels
 */

- (NSDictionary *)loadExpressionWithLevelIndex:(NSUInteger)levelIndex andExpressionIndex:(NSUInteger)expressionIndex;

/*
 *	Saver
 */

- (NSDictionary *)loadSaveAtLevel:(NSUInteger)levelIndex;
- (void)saveLevel:(NSDictionary *)dict atIndex:(NSUInteger)index;
- (void)clearSave;

/*
 *	Count
 */

- (void)saveCoins:(NSUInteger)coins;
- (NSUInteger)loadCoins;
- (NSInteger)countOpened;
- (void)openLevelAtIndex:(NSUInteger)levelIndex;
- (NSString *)nameLevelAtIndex:(NSInteger)index;
- (NSUInteger)countExpressionsInLevel:(NSUInteger)level;

- (BOOL)isFull;
- (void)fullOk;

- (void)trainerIncrement;
- (NSUInteger)trainerCount;

@end
