//
//  ESMStatusView.h
//  Casino
//
//  Created by Sergey Yefanov on 04.08.14.
//  Copyright (c) 2014 Sergey Yefanov. All rights reserved.
//
//  Плашка с полученным количеством монет в данном выражении

#import <UIKit/UIKit.h>

@protocol ESMStatusDelegate;

@interface ESMStatusView : UIView

@property (nonatomic) NSString *message;
@property (nonatomic, weak) id <ESMStatusDelegate> delegate;

- (void)show;
- (void)hide;
- (void)purpose;
- (void)fullVersion;
- (void)win;
- (void)winWithCoins:(NSUInteger)coins;
- (void)winWithCoins:(NSUInteger)record andRecord:(BOOL)isRecord;
- (void)loseWithMorePart:(NSString*)morePart;

@end

@protocol ESMStatusDelegate <NSObject>

- (void) didTouchStatus:(UIView*)view;

@end
