//
//  ExpressionView.m
//  Casino
//
//  Created by Sergey Yefanov on 04.08.14.
//  Copyright (c) 2014 Sergey Yefanov. All rights reserved.
//

#import "ESMExpressionView.h"
#import "ESMFileManager.h"
#import "ESMButtonSign.h"
#import "NSString + Expression.h"
#import "ESMPartOfExpressionView.h"

#define LESS_IMAGE          @"sn_m.png"
#define LESS_IMAGE_FALSE    @"sn_m_false.png"
#define MORE_IMAGE          @"sn_b.png"
#define MORE_IMAGE_FALSE    @"sn_b_false.png"

static NSUInteger const WIDTH_DIGIT_BIG					= 400;     //максимальная ширина в пикселях для бол. выражения
static NSUInteger const WIDTH_DIGIT_NORMAL				= 300;     //максимальная ширина в пикселях для сред. выражения
static NSUInteger const WIDTH_DIGIT_SMALL				= 250;     //максимальная ширина в пикселях для мал. выражения

static NSUInteger const OFFSET_DIGIT_X_LEFT_BIG			= 50;
static NSUInteger const OFFSET_DIGIT_X_LEFT_NORMAL		= 140;
static NSUInteger const OFFSET_DIGIT_X_LEFT_SMAL		= 200;
static NSUInteger const OFFSET_DIGIT_X_RIGHT_BIG		= 570;
static NSUInteger const OFFSET_DIGIT_X_RIGHT_NORMAL		= 580;
static NSUInteger const OFFSET_DIGIT_X_RIGHT_SMALL		= 540;
static NSUInteger const OFFSET_DIGIT_Y					= 10;

static NSUInteger const OFFSET_SIGN_X					= 505;
static NSUInteger const OFFSET_SIGN_MORE_Y				= 40;
static NSUInteger const OFFSET_SIGN_LESS_Y				= 110;

static NSUInteger const OFFSET_PLASHKA_X_BIG			= 5;
static NSUInteger const OFFSET_PLASHKA_X_NORMAL			= 0;
static NSUInteger const OFFSET_PLASHKA_X_SMALL			= 20;

static NSUInteger const NUMBER_DIGIT_FOR_SMALL_PLASHKA	= 4;
static NSUInteger const NUMBER_DIGIT_FOR_NORMAL_PLASHKA = 6;

typedef NS_ENUM (NSUInteger, ESMPlashkaSize) {
	
	ESMPlashkaSizeBig,
	ESMPlashkaSizeNormal,
	ESMPlashkaSizeSmall
	
};

@interface ESMExpressionView () <ESMButtonSignDelegate, ESMPartOfExpressionDelegate>

@property (nonatomic) ESMPartOfExpressionView *	leftPart;
@property (nonatomic) ESMPartOfExpressionView *	rightPart;
@property (nonatomic) UIImageView *				plashka;
@property (nonatomic) ESMButtonSign *			btnMore;
@property (nonatomic) ESMButtonSign *			btnLess;
@property (nonatomic) NSString *				expressionStr;

@end

@implementation ESMExpressionView

#pragma mark - initialization

- (instancetype)init
{
	
	if (self = [super init]) {
		
		[self initialization];
		
	}
	
	return self;
	
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
	
	if (self = [super initWithCoder:aDecoder]) {
		
		[self initialization];
		
	}
	
	return self;
	
}

- (instancetype)initWithFrame:(CGRect)frame
{
	
	if (self = [super initWithFrame:frame]) {
		
		[self initialization];
		
	}
	
	return self;
	
}

- (void)initialization
{
	
	self.plashka = [UIImageView new];
    self.plashka.contentMode = UIViewContentModeCenter;
	[self addSubview:self.plashka];
	
	self.leftPart = [ESMPartOfExpressionView new];
	self.leftPart.delegate = self;
	[self addSubview:self.leftPart];
	
	self.rightPart = [ESMPartOfExpressionView new];
	self.rightPart.delegate = self;
    [self addSubview:self.rightPart];
	
	self.btnMore = [[ESMButtonSign alloc] initWithImage:[UIImage imageNamed:@"sn_b.png"]];
	self.btnMore.delegate = self;
	[self addSubview:self.btnMore];
	
	self.btnLess = [[ESMButtonSign alloc] initWithImage:[UIImage imageNamed:@"sn_m.png"]];
	self.btnLess.delegate = self;
	[self addSubview:self.btnLess];
	
}

#pragma mark - Setters

- (void)setData:(NSDictionary *)data
{
	
	_data = data;
	
	self.leftPart.data = data[@"LeftPart"];
	self.rightPart.data = data[@"RightPart"];
	
	[self p_createView];
	
}

- (void)setExpressionStr:(NSString *)expressionStr
{
	_expressionStr = expressionStr;
	
	NSArray* components = [expressionStr componentsSeparatedByString:@" "];
	
    self.leftPart.expressionStr = [components firstObject];
    self.rightPart.expressionStr = [components lastObject];
	
}

#pragma mark - Getters

- (ESMPlashkaSize)selectedPlashka
{
	
	NSUInteger numberDigit = MAX(self.leftPart.expressionStr.length, self.rightPart.expressionStr.length);
	
    if(numberDigit < NUMBER_DIGIT_FOR_SMALL_PLASHKA) {
		
        return ESMPlashkaSizeSmall;
		
	} else if(numberDigit < NUMBER_DIGIT_FOR_NORMAL_PLASHKA) {
		
        return ESMPlashkaSizeNormal;
		
	}
    
    return ESMPlashkaSizeBig;
	
}

- (NSDictionary *)dictPlashki
{
	
	static NSDictionary *dictPl;
	
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		
		NSDictionary *pathsDict = [[ESMFileManager sharedInstance] objectWithName:@"Images.plist"][@"Plashki"];
		NSMutableDictionary *imagesDict = [NSMutableDictionary new];
		
		[pathsDict enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSString *obj, BOOL *stop) {
			
			[imagesDict setObject:[UIImage imageNamed:obj] forKey:key];
			
		}];
		
		dictPl = imagesDict.copy;
		
	});
	
	return dictPl;
	
}

#pragma mark - Open digit

- (void)openSymbolAtIndex:(NSUInteger)symbolIndex
{
	
	if (symbolIndex > 199) {
		
        @throw [NSException exceptionWithName:@"Error"
									   reason:@"index can not be >199"
									 userInfo:nil];
		
	}
    
    if (symbolIndex < 100) {
		
        return [self.leftPart openSymbolAtIndex:symbolIndex];
		
	} else {
		
        return [self.rightPart openSymbolAtIndex:symbolIndex - 100];
		
	}
	
}

- (void)openSymbolsAtIndexes:(NSArray *)symbolsIndexes
{
	
	NSArray *leftSymbols = [NSArray new];
	NSArray *rightSymbols = [NSArray new];
	
	for (NSNumber *index in symbolsIndexes) {
		
		if (index.integerValue > 199) {
			
			@throw [NSException exceptionWithName:@"Error"
										   reason:@"index can not be >199"
										 userInfo:nil];
			
		} else if (index.integerValue < 100) {
			
			leftSymbols = [leftSymbols arrayByAddingObject:index];
			
		} else {
			
			rightSymbols = [rightSymbols arrayByAddingObject:@(index.integerValue - 100)];
			
		}
		
	}
	
	[self.leftPart openSymbolsAtIndexes:leftSymbols];
	[self.rightPart openSymbolsAtIndexes:rightSymbols];
	
}

- (void)openAllSymbols
{
	
	[self.leftPart openAllSymbols];
	[self.rightPart openAllSymbols];
	
}

#pragma mark - Blink

- (void)blinkAllSymbols
{
	
    [self.rightPart blinkAllSymbols];
    [self.leftPart blinkAllSymbols];
	
}

- (void)blinkSymbolAtIndex:(NSUInteger)symbolIndex
{
	
	if (symbolIndex > 199) {
		
        @throw [NSException exceptionWithName:@"Error"
									   reason:@"index can not be >199"
									 userInfo:nil];
		
	}
    
    if (symbolIndex < 100) {
		
        return [self.leftPart blinkSymbolAtIndex:symbolIndex];
		
	} else {
		
        return [self.rightPart blinkSymbolAtIndex:symbolIndex - 100];
		
	}
	
}

- (void)blinkSymbolsAtIndexes:(NSArray *)symbolsIndexes
{

	NSArray *leftSymbols = [NSArray new];
	NSArray *rightSymbols = [NSArray new];
	
	for (NSNumber *index in symbolsIndexes) {
		
		if (index.integerValue > 199) {
			
			@throw [NSException exceptionWithName:@"Error"
										   reason:@"index can not be >199"
										 userInfo:nil];
			
		} else if (index.integerValue < 100) {
			
			leftSymbols = [leftSymbols arrayByAddingObject:index];
			
		} else {
			
			rightSymbols = [rightSymbols arrayByAddingObject:@(index.integerValue - 100)];
			
		}
		
	}
	
	[self.leftPart blinkSymbolsAtIndexes:leftSymbols];
	[self.rightPart blinkSymbolsAtIndexes:rightSymbols];
	
}

#pragma mark - Signs

- (void)hideLess
{

	[self.btnLess hide];
    [self.btnMore moveToCenter];
	
}

- (void)hideMore
{

	[self.btnMore hide];
    [self.btnLess moveToCenter];

}

- (void)sign:(ESMExpressionViewSign)sign isCorrect:(BOOL)isCorrect
{
	
	if (sign == ESMExpressionViewSignLess && !isCorrect) {
			
		self.btnLess.image = [UIImage imageNamed:LESS_IMAGE_FALSE];
		
	} else if (sign == ESMExpressionViewSignMore && !isCorrect) {
		
		self.btnMore.image = [UIImage imageNamed:MORE_IMAGE_FALSE];
		
	}
	
}

#pragma mark - Expressions

- (void)replaceExpressionsWithLeftPart:(NSString *)leftPart
						  andRightPart:(NSString *)rightPart
{
	
	self.leftPart.expressionStr = leftPart;
	self.rightPart.expressionStr = rightPart;
	
}

#pragma mark - Move karts

- (void)moveKartAtIndex:(NSUInteger)kartIndex
{
	
	if (kartIndex > 199) {
		
        @throw [NSException exceptionWithName:@"Error"
									   reason:@"index can not be >199"
									 userInfo:nil];
		
	}
    
    if (kartIndex < 100) {
		
        return [self.leftPart moveKartAtIndex:kartIndex];
		
	} else {
		
        return [self.rightPart moveKartAtIndex:kartIndex - 100];
		
	}
	
}

- (void)moveKartsAtIndexes:(NSArray *)kartIndexes
{
	
	NSArray *leftSymbols;
	NSArray *rightSymbols;
	
	for (NSNumber *index in kartIndexes) {
		
		if (index.integerValue > 199) {
			
			@throw [NSException exceptionWithName:@"Error"
										   reason:@"index can not be >199"
										 userInfo:nil];
			
		} else if (index.integerValue < 100) {
			
			leftSymbols = [leftSymbols arrayByAddingObject: index];
			
		} else {
			
			rightSymbols = [rightSymbols arrayByAddingObject:@(index.integerValue - 100)];
			
		}
		
	}
	
	[self.leftPart moveKartsAtIndexes:leftSymbols];
	[self.rightPart moveKartsAtIndexes:rightSymbols];
	
}

- (void)moveAllKarts
{
	
    [self.leftPart moveAllKarts];
    [self.rightPart moveAllKarts];
	
}

#pragma mark - Button sign delegate

- (void)buttonSignDidTouch:(ESMButtonSign *)buttonSign
{
	
	if ([self.delegate respondsToSelector:@selector(expressionView:didTouchSign:)]) {
	
		ESMExpressionViewSign sign;
		
		if (buttonSign == self.btnMore) {
			
			sign = ESMExpressionViewSignMore;
			
		} else {
			
			sign = ESMExpressionViewSignLess;
			
		}
		
		[self.delegate expressionView:self didTouchSign:sign];
		
	} else {
		
		NSLog(@"degegate not have selector expressionView:didTouchSign:");
		
	}
	
}

#pragma mark - PartOfExpressionView delegate methods

- (void)partOfExpressionView:(ESMPartOfExpressionView *)partOfExpressionView
			 didTouchAtIndex:(NSUInteger)index
{
	if ([self.delegate respondsToSelector:@selector(expressionView:didTouchSymbolAtIndex:)]) {
		
		NSUInteger newIndex = index;
		
		if (partOfExpressionView == self.rightPart) {
		
			newIndex += 100;
		
		}
		
		[self.delegate expressionView:self didTouchSymbolAtIndex:newIndex];
		
	} else {
		
		NSLog(@"degegate not have selector expressionView:didTouchSymbolAtIndex:");
		
	}
	
}

#pragma mark - Private Methods

- (void)p_createView
{
    UIImage *image;
	
	NSInteger offsetLeftX = 0;
	NSInteger offsetRightX = 0;
	NSInteger offsetY = OFFSET_DIGIT_Y;
	NSUInteger widthDigit = 0;
	NSUInteger offsetPlashkaX = 0;
	NSUInteger offsetSignY = 0;
    
    switch (self.selectedPlashka) {
			
        case ESMPlashkaSizeSmall:
			
            image = self.dictPlashki[@"small"];
			offsetLeftX = OFFSET_DIGIT_X_LEFT_SMAL;
			offsetRightX = OFFSET_DIGIT_X_RIGHT_SMALL;
			widthDigit = WIDTH_DIGIT_SMALL;
			offsetPlashkaX = OFFSET_PLASHKA_X_SMALL;
            break;
            
        case ESMPlashkaSizeNormal:
			
            image = self.dictPlashki[@"normal"];
			offsetLeftX = OFFSET_DIGIT_X_LEFT_NORMAL;
			offsetRightX = OFFSET_DIGIT_X_RIGHT_NORMAL;
			widthDigit = WIDTH_DIGIT_NORMAL;
			offsetPlashkaX = OFFSET_PLASHKA_X_NORMAL;
            break;
            
        case ESMPlashkaSizeBig:
			
            image = self.dictPlashki[@"big"];
			offsetLeftX = OFFSET_DIGIT_X_LEFT_BIG;
			offsetRightX = OFFSET_DIGIT_X_RIGHT_BIG;
			widthDigit = WIDTH_DIGIT_BIG;
			offsetPlashkaX = OFFSET_PLASHKA_X_BIG;
			offsetSignY = 15;
            break;
            
        default:
            break;
    }
	
	self.leftPart.frame = CGRectMake(offsetLeftX, offsetY, widthDigit, 200);
	self.rightPart.frame = CGRectMake(offsetRightX, offsetY, widthDigit, 200);
	
	self.btnMore.frame = CGRectMake(OFFSET_SIGN_X, OFFSET_SIGN_MORE_Y - offsetSignY, 50, 50);
	self.btnLess.frame = CGRectMake(OFFSET_SIGN_X, OFFSET_SIGN_LESS_Y - offsetSignY, 50, 50);
    
	self.plashka.frame = CGRectMake(offsetPlashkaX, 10, 1004, 200);
    self.plashka.image = image;
	
	[self sendSubviewToBack:self.plashka];
	
}

@end
