//
//  LevelViewController.h
//  Casino
//
//  Created by Sergey Yefanov on 04.08.14.
//  Copyright (c) 2014 Sergey Yefanov. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, ESMGameMode) {
	
	ESMGameModeGame,
	ESMGameModeTrainer
	
};

@interface ESMGameVC : UIViewController

@property (nonatomic) NSUInteger levelIndex;
@property (nonatomic) ESMGameMode gameMode;

@end