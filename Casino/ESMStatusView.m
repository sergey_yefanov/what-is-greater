//
//  ESMWinStatusView.m
//  Casino
//
//  Created by Sergey Yefanov on 04.08.14.
//  Copyright (c) 2014 Sergey Yefanov. All rights reserved.
//

#define PLASHKA_NAME @"purpose_pl.png"

#import "ESMStatusView.h"

@interface ESMStatusView ()

@property (nonatomic) UIImageView *	imagePl;
@property (nonatomic) UILabel *		labelMessage;

@end

@implementation ESMStatusView

#pragma mark - initialize

- (instancetype)init {
	
	if (self = [super init]) {
		
		[self initialize];
		
	}
	
	return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
	
	if (self = [super initWithFrame:frame]) {
		
		[self initialize];
		
	}
	
	return self;
	
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
	
	if (self = [super initWithCoder:aDecoder]) {
		
		[self initialize];
		
	}
	
	return self;
	
}

- (void)initialize
{
	
	self.imagePl = [[UIImageView alloc] initWithImage:[UIImage imageNamed:PLASHKA_NAME]];
	self.imagePl.contentMode = UIViewContentModeCenter;
	self.imagePl.frame = self.bounds;
	self.backgroundColor = [UIColor clearColor];
	
	self.labelMessage = [[UILabel alloc] initWithFrame:CGRectMake(0, -5, self.frame.size.width, self.frame.size.height)];
	self.labelMessage.textAlignment = NSTextAlignmentCenter;
	self.labelMessage.contentMode = UIViewContentModeCenter;
	self.labelMessage.backgroundColor = [UIColor clearColor];
	
	[self addSubview:self.imagePl];
	[self addSubview:self.labelMessage];
	
}

#pragma mark - Texts

- (void)purpose
{
	
	self.labelMessage.text = NSLocalizedString(@"purpose", @"");
	
}

- (void)fullVersion
{
	
	self.labelMessage.text = NSLocalizedString(@"buy message", nil);
	
}


- (void)win
{

	self.labelMessage.text = NSLocalizedString(@"win message", nil);
	
}

- (void)winWithCoins:(NSUInteger)coins
{
	
    NSString *coinStr;
    
    switch (coins % 10) {
        case 0:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
            coinStr = [NSString stringWithFormat:NSLocalizedString(@"5coin", @"")];
            break;
        case 1:
            coinStr = [NSString stringWithFormat:NSLocalizedString(@"1coin", @"")];
            break;
        case 2:
        case 3:
        case 4:
            coinStr = [NSString stringWithFormat:NSLocalizedString(@"2coin", @"")];
            break;
            
        default:
            break;
    }
    
    self.labelMessage.text = [NSString stringWithFormat:NSLocalizedString(@"your win is %d %@.", @""), coins, coinStr];
}

- (void)winWithCoins:(NSUInteger)record andRecord:(BOOL)isRecord
{
    
    if (isRecord) {
		
        self.labelMessage.text = NSLocalizedString(@"this is record", @"");
		
	}
    
    NSString *monetStr;
    
    switch (record % 10) {
			
        case 0:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
            monetStr = [NSString stringWithFormat:NSLocalizedString(@"5coin", @"")];
            break;
        case 1:
            monetStr = [NSString stringWithFormat:NSLocalizedString(@"1coin", @"")];
            break;
        case 2:
        case 3:
        case 4:
            monetStr = [NSString stringWithFormat:NSLocalizedString(@"2coin", @"")];
            break;
            
        default:
            break;
			
    }
    
	self.labelMessage.text = [NSString stringWithFormat:NSLocalizedString(@"record is %d %@. click for record", @""), record, monetStr];
	
}

- (void)loseWithMorePart:(NSString*)morePart
{
    
    if ([morePart isEqual:@"LeftPart"]) {
		
        self.labelMessage.text = NSLocalizedString(@"fail, left part is more", @"");
        return;
		
    } else if ([morePart isEqual:@"RightPart"]) {
		
        self.labelMessage.text = NSLocalizedString(@"fail, right part is more", @"");
        return;
		
    }
    
    self.labelMessage.text = @"";
}

#pragma mark - Controls

- (void)show
{
	
	[UIView animateWithDuration:0.5f
						  delay:0.0f
						options:UIViewAnimationOptionCurveLinear
					 animations:^{
						 
						 self.alpha = 1.0f;
						 self.labelMessage.alpha = 1.0f;
						 
					 }
					 completion:nil];
	
}

- (void)hide
{
	
	self.labelMessage.text = @"";
	self.alpha = 0.0f;
	self.labelMessage.alpha = 0.0f;
	
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	
    if ([self.delegate respondsToSelector:@selector(didTouchStatus:)]) {
		
        [self.delegate didTouchStatus:self];
		
    } else {
		
        NSLog(@"delegate have not selector didTouchStatus:");
		
	}
}

@end
