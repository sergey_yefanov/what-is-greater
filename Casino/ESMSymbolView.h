//
//  ESMSymbolView.h
//  Casino
//
//  Created by Sergey Yefanov on 04.08.14.
//  Copyright (c) 2014 Sergey Yefanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ESMSymbolViewDelegate;

@interface ESMSymbolView : UIView

@property (nonatomic, getter = isOpen)	BOOL						open;
@property (nonatomic)					NSString *					symbol;
@property (nonatomic)					id <ESMSymbolViewDelegate>  delegate;

- (void)open;
- (void)move;
- (void)blink;

@end

@protocol ESMSymbolViewDelegate <NSObject>

- (void)symbolDidTouch:(ESMSymbolView *)symbolView;

@end