//
//  ButtonSign.h
//  Casino
//
//  Created by Sergey Yefanov on 04.08.14.
//  Copyright (c) 2014 Sergey Yefanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ESMButtonSignDelegate;

@interface ESMButtonSign : UIImageView

@property (nonatomic, weak) id <ESMButtonSignDelegate> delegate;

- (void)moveToCenter;
- (void)hide;

@end

@protocol ESMButtonSignDelegate <NSObject>

- (void)buttonSignDidTouch:(ESMButtonSign *)buttonSign;

@end
