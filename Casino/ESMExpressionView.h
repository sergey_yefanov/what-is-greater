//
//  ExpressionView.h
//  Casino
//
//  Created by Sergey Yefanov on 04.08.14.
//  Copyright (c) 2014 Sergey Yefanov. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM (NSUInteger, ESMExpressionViewSign){
	
	ESMExpressionViewSignMore,
	ESMExpressionViewSignLess
	
};

@protocol ESMExpressionViewDelegate;

@interface ESMExpressionView : UIView

@property (nonatomic)			NSDictionary *					data;
@property (nonatomic, readonly) NSString *						expressionStr;
@property (nonatomic, weak)		id <ESMExpressionViewDelegate>	delegate;

- (void)blinkSymbolAtIndex:(NSUInteger)symbolIndex;
- (void)blinkSymbolsAtIndexes:(NSArray *)symbolsIndexes;
- (void)blinkAllSymbols;

- (void)openSymbolAtIndex:(NSUInteger)symbolIndex;
- (void)openSymbolsAtIndexes:(NSArray *)symbolsIndexes;
- (void)openAllSymbols;

- (void)moveKartAtIndex:(NSUInteger)kartIndex;
- (void)moveKartsAtIndexes:(NSArray *)kartIndexes;
- (void)moveAllKarts;

- (void)hideLess;
- (void)hideMore;
- (void)sign:(ESMExpressionViewSign)sign isCorrect:(BOOL)isCorrect;

- (void)replaceExpressionsWithLeftPart:(NSString *)leftPart
						  andRightPart:(NSString *)rightPart;

@end

@protocol ESMExpressionViewDelegate <NSObject>

- (void)expressionView:(ESMExpressionView *)expressionView didTouchSign:(ESMExpressionViewSign)sign;
- (void)expressionView:(ESMExpressionView *)expressionView didTouchSymbolAtIndex:(NSUInteger)index;

@end
