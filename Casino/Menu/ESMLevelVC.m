//
//  MenuViewController.m
//  Casino
//
//  Created by Sergey Yefanov on 04.08.14.
//  Copyright (c) 2014 Sergey Yefanov. All rights reserved.
//

#import "ESMLevelVC.h"
#import "ESMLevelView.h"
#import "ESMSaveManager.h"
#import "ESMFileManager.h"
#import "ESMGameVC.h"
#import "ESMKoshelFrontView.h"
#import "ESMKoshelBackView.h"
#import "ESMCoinView.h"
#import "ESMPayManager.h"

@interface ESMLevelVC () <ESMLevelViewDelegate>

@property (nonatomic, weak) IBOutlet	UIScrollView *			scrollView;
@property (nonatomic)					NSUInteger				countLevels;
@property (nonatomic)					NSInteger				countOpened;
@property (nonatomic)					NSArray *				levels;
@property (nonatomic)					ESMKoshelFrontView *	koshelFrontView;
@property (nonatomic)					ESMKoshelBackView *		koshelBackView;
@property (nonatomic)					ESMCoinView *			coinsView;
@property (nonatomic)					UIButton *				btnMenu;

@end

@implementation ESMLevelVC

- (void)viewDidLoad
{
	
	[super viewDidLoad];
	
	self.levels = [NSArray new];
	self.countLevels = [[ESMSaveManager sharedInstance] countLevels];
	
	
	for (NSUInteger i = 0; i < self.countLevels; i++) {
		
		ESMLevelView *levelView = [[ESMLevelView alloc] initWithFrame:(CGRectMake(i * 1024, 0, 1024, 768))];
		levelView.levelIndex = i;
		levelView.levelName = [[ESMSaveManager sharedInstance] nameLevelAtIndex:i];
		levelView.delegate = self;
		levelView.backgroundColor = [UIColor colorWithPatternImage:self.backgroundsArr[i]];
		[self.scrollView addSubview:levelView];
		self.levels = [self.levels arrayByAddingObject:levelView];
		
	}
	
	self.scrollView.pagingEnabled = YES;
	self.scrollView.bounces = NO;
	
	// koshel back view
	
	self.koshelBackView = [[ESMKoshelBackView alloc] initWithFrame:CGRectMake(0, 540, 300, 200)];
	[self.view addSubview:self.koshelBackView];
	
	// coins view
	
	self.coinsView = [[ESMCoinView alloc] initWithFrame:CGRectMake(120, 550, 300, 200)];
	[self.view addSubview:self.coinsView];
	
	// koshel front view
	
	self.koshelFrontView = [[ESMKoshelFrontView alloc] initWithFrame:CGRectMake(0, 540, 300, 200)];
	self.koshelFrontView.countCoins = [[ESMSaveManager sharedInstance] loadCoins];
	[self.view addSubview:self.koshelFrontView];
	
	// button menu
	self.btnMenu = [UIButton buttonWithType:UIButtonTypeCustom];
	self.btnMenu.frame = CGRectMake(10, 10, 150, 100);
	[self.btnMenu setImage:[UIImage imageNamed:@"back_to_menu.png"] forState:UIControlStateNormal];
	[self.btnMenu addTarget:self action:@selector(touchMenu) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:self.btnMenu];
	
}

- (void)viewWillAppear:(BOOL)animated
{
	
	[super viewWillAppear:animated];
	[self update];
	
}

#pragma mark - Update

- (void)update
{
	
	self.countOpened = [[ESMSaveManager sharedInstance] countOpened];
	
	if (self.countOpened >= self.countLevels) {
		
				self.scrollView.contentSize = CGSizeMake(1024 * self.countOpened, 768);
		
	} else {
		
		self.scrollView.contentSize = CGSizeMake(1024 * (self.countOpened + 1), 768);
		
	}
	
	for (NSInteger i = 0; i < self.countOpened + 1; i++) {
		
		if (i < self.countLevels) {
			
			[((ESMLevelView *) self.levels[i]) update];
			
		}
		
	}
	
	self.koshelFrontView.countCoins = [[ESMSaveManager sharedInstance] loadCoins];
	
}

#pragma mark - Getters

- (NSArray *)backgroundsArr
{
	
	static NSArray *imagesArr;
	static dispatch_once_t onceToken;
	
	dispatch_once(&onceToken, ^{
		
		NSArray *arrPaths = [[[ESMFileManager sharedInstance] objectWithName:@"Images.plist"] objectForKey:@"Backgrounds"];
		imagesArr = [NSArray new];
		
		for (NSString *imagePath in arrPaths) {
			
			imagesArr = [imagesArr arrayByAddingObject:[UIImage imageNamed:imagePath]];
			
		}
		
	});
	
	return imagesArr;
	
}

#pragma mark - Interface methods

#pragma mark -

-(void) loadLevel:(NSInteger) numberLevel
{
	
//    LevelViewController *levelViewController = [[LevelViewController alloc] initWithNibName:@"LevelViewController" bundle:nil];
//    levelViewController.thisLevel = numberLevel;

//    [self.navigationController pushViewController:levelViewController animated:YES];
	
}

- (void) startBlink
{
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:1.0f];
//    [UIView setAnimationRepeatCount:100.0f];
//    [UIView setAnimationCurve:UIViewAnimationCurveLinear];
//    [UIView setAnimationDelegate:self];
//    levelIcon.alpha = 0.0;
//    [UIView commitAnimations];
//    
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:1.0f];
//    [UIView setAnimationRepeatCount:100.0f];
//    [UIView setAnimationCurve:UIViewAnimationCurveLinear];
//    [UIView setAnimationDelegate:self];
//    levelIcon.alpha = 1.0;
//    [UIView commitAnimations];
}

- (void) stopBlink
{
    
}

- (void)setPercent:(NSUInteger)countStar
{
//    [menuLevelStars setPercent:countStar];
}

#pragma mark - Menu button

- (void)touchMenu
{
	
	[self.navigationController popViewControllerAnimated:YES];
	
}

#pragma mark - Level view delegate methods

- (void)didTouchPlay:(ESMLevelView *)levelView
{
	
	NSUInteger index = [self.levels indexOfObject:levelView];
	
	ESMGameVC *gameVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ESMGameVC"];
	gameVC.gameMode = ESMGameModeGame;
	gameVC.levelIndex = index;
    
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @(index + 1), @"Level",
                                   nil];
    
    [Flurry logEvent:@"LEVEL_RUNNED" withParameters:articleParams timed:YES];
	
	[self.navigationController pushViewController:gameVC animated:YES];
	
}

- (void)didTouchPayFromKoshel:(ESMLevelView *)levelView
{

	NSInteger index = [self.levels indexOfObject:levelView];
	NSDictionary *saveData = [[ESMSaveManager sharedInstance] loadSaveAtLevel:index];
	NSInteger coins = [[ESMSaveManager sharedInstance] loadCoins];
	NSInteger cost = ((NSNumber *)saveData[@"Cost"]).integerValue;
    
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @(index + 1), @"Level",
                                   nil];
    
    [Flurry logEvent:@"BUY_KOSHEL_TOUCH" withParameters:articleParams];
	
	coins -= cost;
	
	if (coins >= 0) {
        
        [Flurry logEvent:@"BUY_KOSHEL_OK" withParameters:articleParams];
		
		[self.koshelBackView open];
		[self.koshelFrontView open];
		
		__block id observerCoins = [[NSNotificationCenter defaultCenter] addObserverForName:ESM_COINS_MOVED_NOTIFICATION
														  object:[self.coinsView class]
														   queue:nil
													  usingBlock:^(NSNotification *note) {
														  
														  [[ESMSaveManager sharedInstance] openLevelAtIndex:index];
														  [[ESMSaveManager sharedInstance] saveCoins:coins];
														  [self.koshelBackView close];
														  [self.koshelFrontView close];
														  [[NSNotificationCenter defaultCenter] removeObserver:observerCoins];
														  
													  }];
		
		__block id observerKoshel = [[NSNotificationCenter defaultCenter] addObserverForName:ESM_KOSHEL_CLOSED_NOTIFICATION
																				object:[self.koshelFrontView class]
																				 queue:nil
																			usingBlock:^(NSNotification *note) {
																				
																				[self update];
																				[[NSNotificationCenter defaultCenter] removeObserver:observerKoshel];
																				
																			}];
		
		[self.coinsView createCoins:4 withOnePosition:YES];
		[self.coinsView moveAllCoinsToCenter];
		
	} else {
		
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
															message:NSLocalizedString(@"Koshel have not count monets", nil)
														   delegate:nil
												  cancelButtonTitle:@"ОК"
												  otherButtonTitles:nil, nil];
		[alertView show];
		
	}
	
}

- (void)didTouchPayFromApple:(ESMLevelView *)levelView
{
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @"Game", @"place",
                                   nil];
    
    [Flurry logEvent:@"GAME_BUY_TOUCH" withParameters:articleParams];
	[SVProgressHUD setBackgroundColor:[UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:0.5]];
	[SVProgressHUD setForegroundColor:[UIColor whiteColor]];
	[SVProgressHUD show];
	
	__block id observerTransaction =  [[NSNotificationCenter defaultCenter] addObserverForName:ESM_TRANSACTION_FALIED
																						object:nil
																						 queue:nil
																					usingBlock:^(NSNotification *note) {
																						
																						dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
																							// time-consuming task
																							dispatch_sync(dispatch_get_main_queue(), ^{
																								[SVProgressHUD dismiss];
																							});
																						});
																						
																						[[NSNotificationCenter defaultCenter] removeObserver:observerTransaction];
                                                                                        [Flurry logEvent:@"GAME_BUY_Fail" withParameters:articleParams];
																						UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil) message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
																						[alertView show];
																						
																					}];
	
	__block id observer =  [[NSNotificationCenter defaultCenter] addObserverForName:ESM_FULL_VERSION_PURCHASED
																			 object:nil
																			  queue:nil
																		 usingBlock:^(NSNotification *note) {
																			 
																			dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
																				// time-consuming task
																				dispatch_async(dispatch_get_main_queue(), ^{
																					[SVProgressHUD dismiss];
																				});
																			});
                                                                             [Flurry logEvent:@"GAME_BUY_OK" withParameters:articleParams];
																			 [[ESMSaveManager sharedInstance] fullOk];
																			 [self update];
																			 [[NSNotificationCenter defaultCenter] removeObserver:observer];
																			 
																		 }];
	
	[[ESMPayManager sharedInstance] buyFullVersion];
	
}

- (void)didTouchRestorePurchase:(ESMLevelView *)levelView
{
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @"Game", @"place",
                                   nil];
    
    [Flurry logEvent:@"GAME_BUY_TOUCH" withParameters:articleParams];
    [SVProgressHUD setBackgroundColor:[UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:0.5]];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    [SVProgressHUD show];
    
    __block id observerTransaction =  [[NSNotificationCenter defaultCenter] addObserverForName:ESM_TRANSACTION_FALIED
                                                                                        object:nil
                                                                                         queue:nil
                                                                                    usingBlock:^(NSNotification *note) {
                                                                                        
                                                                                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                                                                            // time-consuming task
                                                                                            dispatch_sync(dispatch_get_main_queue(), ^{
                                                                                                [SVProgressHUD dismiss];
                                                                                            });
                                                                                        });
                                                                                        
                                                                                        [[NSNotificationCenter defaultCenter] removeObserver:observerTransaction];
                                                                                        [Flurry logEvent:@"GAME_BUY_Fail" withParameters:articleParams];
                                                                                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil) message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                                                                        [alertView show];
                                                                                        
                                                                                    }];
    
    __block id observer =  [[NSNotificationCenter defaultCenter] addObserverForName:ESM_FULL_VERSION_PURCHASED
                                                                             object:nil
                                                                              queue:nil
                                                                         usingBlock:^(NSNotification *note) {
                                                                             
                                                                             dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                                                                 // time-consuming task
                                                                                 dispatch_async(dispatch_get_main_queue(), ^{
                                                                                     [SVProgressHUD dismiss];
                                                                                 });
                                                                             });
                                                                             [Flurry logEvent:@"GAME_BUY_OK" withParameters:articleParams];
                                                                             [[ESMSaveManager sharedInstance] fullOk];
                                                                             [self update];
                                                                             [[NSNotificationCenter defaultCenter] removeObserver:observer];
                                                                             
                                                                         }];
    
    [[ESMPayManager sharedInstance] restorePurchace];
}

@end
