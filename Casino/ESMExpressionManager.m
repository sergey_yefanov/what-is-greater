//
//  Expression.m
//  Casino
//
//  Created by Sergey Yefanov on 04.08.14.
//  Copyright (c) 2014 Sergey Yefanov. All rights reserved.
//

#import "ESMExpressionManager.h"
#import "ESMPartOfExpressionManager.h"

@interface ESMExpressionManager ()

@property (nonatomic) ESMPartOfExpressionManager *	leftPart;
@property (nonatomic) ESMPartOfExpressionManager *	rightPart;
@property (nonatomic) NSUInteger					record;
@property (nonatomic) NSString *					recordCombination;
@property (nonatomic) NSString *					morePart;

@end

@implementation ESMExpressionManager

#pragma mark - Initialization

+ (instancetype)sharedInstance
{
	
	static id expressionManager = nil;
	static dispatch_once_t token;
	
	dispatch_once(&token, ^{
		
		expressionManager = [self new];
		
	});
	
	return expressionManager;
	
}

- (instancetype)init
{
	
	if (self = [super init]) {
		
		[self initialization];
		
	}
	
	return self;
	
}

- (void)initialization
{
	
	self.leftPart = [ESMPartOfExpressionManager new];
	self.rightPart = [ESMPartOfExpressionManager new];
	
}

#pragma mark - setters

- (void)setData:(NSDictionary *)data{
	
	_data = data;
	
	self.leftPart.data		= data[@"LeftPart"];
	self.rightPart.data		= data[@"RightPart"];
	self.morePart			= data[@"MorePart"];
	self.record				= ((NSNumber *)data[@"Record"]).integerValue;
	self.recordCombination	= data[@"RecordCombination"];
	
}

#pragma mark - getters

- (NSUInteger)numberCloseDigit
{
	
    return [self.leftPart numberOfCloseDigit] + [self.rightPart numberOfCloseDigit];
	
}

- (BOOL)leftPartIsMore
{
    
	return self.leftPart.summMin >= self.rightPart.summMax;
	
}

- (BOOL)leftPartIsLess
{
    
    return self.leftPart.summMax <= self.rightPart.summMin;
	
}

- (BOOL)rightPartIsLess
{
	
	return ![self leftPartIsLess];
	
}

- (BOOL)rightPartIsMore
{
	
	return ![self leftPartIsMore];
	
}

#pragma mark - Open symbols

- (BOOL)openSymbolAtIndex:(NSUInteger)index
{
	
    if (index > 199) {
		
        @throw [NSException exceptionWithName:@"Error"
									   reason:@"index can not be >199"
									 userInfo:nil];
		
	}
    
    if (index < 100) {
		
        return [self.leftPart openDigitAtIndex:index];
		
	} else {
		
        return [self.rightPart openDigitAtIndex:index - 100];
	}
	
}

#pragma mark -

- (NSDictionary *)stringsWithMorePart:(NSString *)morePart
{
	
	NSString *leftPartString;
	NSString *rightPartString;
	
    if([morePart isEqualToString:@"left"]) {
		
		leftPartString = self.leftPart.stringMax;
		rightPartString = self.rightPart.stringMin;


    } else if([morePart isEqualToString:@"right"]) {
		
		leftPartString = self.leftPart.stringMin;
		rightPartString = self.rightPart.stringMax;

    }
	
	return @{@"LeftPart" : leftPartString, @"RightPart" : rightPartString};
	
}

@end
