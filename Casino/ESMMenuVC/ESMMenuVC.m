//
//  ESMMenuVC.m
//  Casino
//
//  Created by Ефанов Сергей on 05.08.14.
//
//

#import "ESMMenuVC.h"
#import "ESMLevelVC.h"
#import "ESMSettingsVC.h"
#import "ESMGameVC.h"
#import "ESMFileManager.h"

@interface ESMMenuVC () <UIAlertViewDelegate>

@property (nonatomic) UIButton *	btnGame;
@property (nonatomic) UIButton *	btnTrainer;
@property (nonatomic) UIButton *	btnReset;
@property (nonatomic) UILabel *		nameLabel;

@end

@implementation ESMMenuVC

- (void)viewDidLoad
{
	
	[super viewDidLoad];
	
	// name label
	
	self.nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 150, 1004, 110)];
	self.nameLabel.textAlignment = NSTextAlignmentCenter;
	NSArray *fonts = [UIFont fontNamesForFamilyName:@"Nautilus Pompilius"];
	self.nameLabel.font = [UIFont fontWithName:fonts.firstObject size:98.];
	self.nameLabel.shadowColor = [UIColor blackColor];
	self.nameLabel.shadowOffset = CGSizeMake(0.3f, 0.3f);
	self.nameLabel.textColor = [UIColor whiteColor];
	self.nameLabel.text = NSLocalizedString(@"What Is Greater?", nil);
	self.nameLabel.numberOfLines = 1;
	[self.view addSubview:self.nameLabel];
	
	// button game
	self.btnGame = [UIButton buttonWithType:UIButtonTypeCustom];
	self.btnGame.frame = CGRectMake(400, 350, 230, 80);
	[self.btnGame setBackgroundImage:[UIImage imageNamed:@"4_2.png"] forState:UIControlStateNormal];
	[self.btnGame setTitle:NSLocalizedString(@"Game", nil) forState:UIControlStateNormal];
	[self.btnGame setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[self.btnGame addTarget:self action:@selector(pushPlay) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:self.btnGame];
	
	// button trainer
	self.btnTrainer = [UIButton buttonWithType:UIButtonTypeCustom];
	self.btnTrainer.frame = CGRectMake(400, 490, 230, 80);
	[self.btnTrainer setBackgroundImage:[UIImage imageNamed:@"4_2.png"] forState:UIControlStateNormal];
	[self.btnTrainer setTitle:NSLocalizedString(@"Training", nil) forState:UIControlStateNormal];
	[self.btnTrainer setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[self.btnTrainer addTarget:self action:@selector(pushTrainer) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:self.btnTrainer];
	
	// button settings
	self.btnReset = [UIButton buttonWithType:UIButtonTypeCustom];
	self.btnReset.contentMode = UIViewContentModeCenter;
	self.btnReset.frame = CGRectMake(916, 10, 100, 100);
	[self.btnReset setBackgroundImage:[UIImage imageNamed:@"меню5.png"] forState:UIControlStateNormal];
	[self.btnReset addTarget:self action:@selector(pushReset) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:self.btnReset];
	
	self.view.backgroundColor = [UIColor colorWithPatternImage:self.backgroundsArr[0]];
	
}

#pragma mark - Getters

- (NSArray *)backgroundsArr
{
	
	static NSArray *imagesArr;
	static dispatch_once_t onceToken;
	
	dispatch_once(&onceToken, ^{
		
		NSArray *arrPaths = [[ESMFileManager sharedInstance] objectWithName:@"Images.plist"][@"Backgrounds"];
		imagesArr = [NSArray new];
		
		for(NSString *imagePath in arrPaths) {
			
			imagesArr = [imagesArr arrayByAddingObject:[UIImage imageNamed:imagePath]];
			
		}
		
	});
	
	return imagesArr;
	
}

- (void)pushPlay
{
	[Flurry logEvent:@"LOG_GAME_TOUCH"];
	ESMLevelVC *levelVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ESMLevelVC"];
    
	[self.navigationController pushViewController:levelVC
                                         animated:YES];
	
}

- (void)pushTrainer
{
    [Flurry logEvent:@"LOG_TRAINER_TOUCH" timed:YES];
	ESMGameVC *gameVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ESMGameVC"];
	gameVC.gameMode = ESMGameModeTrainer;
    
	[self.navigationController pushViewController:gameVC
                                         animated:YES];
	
}

- (void)pushReset
{
    [Flurry logEvent:@"LOG_RESET_TOUCH"];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil
                                                        message:NSLocalizedString(@"Clear save?", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"Clear", nil)
                                              otherButtonTitles:NSLocalizedString(@"Cancel", nil), nil];
    [alertView show];

}

- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (buttonIndex == 0)
    {
        [Flurry logEvent:@"LOG_RESET_TOUCH_CLEAR"];
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:1.0f];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft
                               forView:self.view
                                 cache:NO];
        [UIView commitAnimations];
        
        [[ESMSaveManager sharedInstance] clearSave];
    }
    
}

@end
