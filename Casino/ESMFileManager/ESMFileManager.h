//
//  ESMFileManager.h
//  Casino
//
//  Created by Sergey Yefanov on 04.08.14.
//  Copyright (c) 2014 Sergey Yefanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ESMFileManager : NSObject

+ (instancetype)sharedInstance;
- (id)objectWithName:(NSString *)name;

@end
