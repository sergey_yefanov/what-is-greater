//
//  ESMFileManager.m
//  Casino
//
//  Created by Sergey Yefanov on 04.08.14.
//  Copyright (c) 2014 Sergey Yefanov. All rights reserved.
//

#import "ESMFileManager.h"

@implementation ESMFileManager

+ (instancetype)sharedInstance {
	
	static ESMFileManager *fileManager = nil;
	static dispatch_once_t token;
	
	dispatch_once(&token, ^{
		
		fileManager = [self new];
		
	});
	
	return fileManager;
}

- (id)objectWithName:(NSString *)name {
	
	if (!name) {
		
		return nil;
		
	}
	
	NSArray *components = [name componentsSeparatedByString:@"."];
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                              NSUserDomainMask, YES) objectAtIndex:0];
    NSString *plistPath = [rootPath stringByAppendingPathComponent:name];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath]) {
		
        plistPath = [[NSBundle mainBundle] pathForResource:[components firstObject] ofType:[components lastObject]];
		
	}
    
    NSData *plistXML = [[NSFileManager defaultManager] contentsAtPath:plistPath];
	
	NSString *errorDesc = nil;
    NSPropertyListFormat format;
	
    id object = [NSPropertyListSerialization
							  propertyListFromData:plistXML
							  mutabilityOption:NSPropertyListImmutable
							  format:&format
							  errorDescription:&errorDesc];
	
	if (!object) {
		
        NSLog(@"Error reading plist: %@, format: %u", errorDesc, format);
		
	}
	
	return object;
}

@end
