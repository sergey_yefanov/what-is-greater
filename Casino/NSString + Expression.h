//
//  StringsUtilities.h
//  Casino
//
//  Created by Sergey Yefanov on 04.08.14.
//  Copyright (c) 2014 Sergey Yefanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Expression)

@property (nonatomic, readonly) BOOL isDigit;
@property (nonatomic, readonly) BOOL isPlus;
@property (nonatomic, readonly) BOOL isMinus;
@property (nonatomic, readonly) BOOL isMultiply;
@property (nonatomic, readonly) BOOL isDivision;
@property (nonatomic, readonly) BOOL isSign;
@property (nonatomic, readonly) BOOL isBracket;
@property (nonatomic, readonly) BOOL isCloseBracket;
@property (nonatomic, readonly) BOOL isOpenBracket;

/*
 * Parse string
 */

- (NSUInteger)binUInteger;
- (NSArray *)binArray;
- (NSArray *)trueIndexesArray;
- (BOOL)binBool;

/*
 * --
 */

- (NSUInteger)numberOfDigit;
- (NSUInteger)numberOfSign;
- (NSUInteger)numberOfBracket;
- (NSString *)symbolAtIndex:(NSUInteger)index;
- (BOOL)binAtIndex:(NSUInteger) index;
+ (NSString *)stringWithBinDigit:(NSUInteger)digit andNumberSymbol:(NSInteger)numberSymbol;
- (NSString *)cutLeftPart; //Вырезает левую часть из полученной строки
- (NSString *)cutRightPart; //Вырезает правую часть из полученной строки
- (NSString *)cutTerm;
- (NSString *)cutDigit;
- (NSString *)insertBracket;
- (NSRange)findDigitWithStart;
- (NSUInteger)findDigitAtIndex:(NSUInteger)index;

@end
