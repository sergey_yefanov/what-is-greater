//
//  ESMKoshelBackView.h
//  Casino
//
//  Created by Ефанов Сергей on 04.08.14.
//
//

#import <UIKit/UIKit.h>

@interface ESMKoshelBackView : UIView

- (void)open;
- (void)close;

@end
