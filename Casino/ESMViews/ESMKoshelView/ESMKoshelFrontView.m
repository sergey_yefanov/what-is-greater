//
//  ESMKoshelFrontView.m
//  Casino
//
//  Created by Ефанов Сергей on 04.08.14.
//
//

#import "ESMKoshelFrontView.h"
#import "ESMFileManager.h"

static CGFloat const ANIMATION_DURATION			 = 0.5;

NSString * const ESM_KOSHEL_OPENNED_NOTIFICATION = @"ESM_KOSHEL_OPENNED_NOTIFICATION";
NSString * const ESM_KOSHEL_CLOSED_NOTIFICATION	 = @"ESM_KOSHEL_CLOSED_NOTIFICATION";

@interface ESMKoshelFrontView()

@property (nonatomic) UILabel *		labelCoins;
@property (nonatomic) NSArray *		imagesFront;

@end

@implementation ESMKoshelFrontView

#pragma mark - initialization

- (instancetype)init
{
	
	if (self = [super init]) {
		
		[self initialize];
		
	}
	
	return self;
	
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
	
	if (self = [super initWithCoder:aDecoder]) {
		
		[self initialize];
		
	}
	
	return self;
	
}

- (instancetype)initWithFrame:(CGRect)frame
{
	
	if (self = [super initWithFrame:frame]) {
		
		[self initialize];
		
	}
	
	return self;
	
}

- (void)initialize
{
	NSMutableArray *mImagesView = [NSMutableArray new];
	
	[self.imagesForAnimationFront enumerateObjectsUsingBlock:^(UIImage *image, NSUInteger idx, BOOL *stop) {
		
		UIImageView *imageView = [[UIImageView alloc] initWithFrame:self.bounds];
		imageView.image = image;
		imageView.contentMode = UIViewContentModeCenter;
		imageView.alpha = 0.0f;
		[self addSubview:imageView];
		[mImagesView addObject:imageView];
		
	}];
	
	self.imagesFront = mImagesView.copy;
	
	((UIImageView *)self.imagesFront[0]).alpha = 1.0f;
	
	self.labelCoins = [[UILabel alloc] initWithFrame:self.bounds];
	self.labelCoins.font = [UIFont fontWithName:@"Arial" size:50.];
		self.labelCoins.textAlignment = NSTextAlignmentCenter;
	self.labelCoins.textColor = [UIColor grayColor];
	self.labelCoins.backgroundColor = [UIColor clearColor];
	self.labelCoins.text = @"0";
	self.labelCoins.numberOfLines = 1;
	
	self.backgroundColor = [UIColor clearColor];
	
	[self addSubview:self.labelCoins];
	
}

- (NSArray *)imagesForAnimationFront
{
	
	static NSArray *arrAnimation;
	static dispatch_once_t onceToken;
	
	dispatch_once(&onceToken, ^{
		
		NSArray *namesImages = [[ESMFileManager sharedInstance] objectWithName:@"Images.plist"][@"Koshel"][@"Front"];
		arrAnimation = [NSArray new];
		
		for(NSString *imageName in namesImages) {
			
			arrAnimation = [arrAnimation arrayByAddingObject:[UIImage imageNamed:imageName]];
			
		}
		
	});
	
	return arrAnimation;
}

#pragma mark - public methods

-(void) open
{
	
    [self p_animateOpen];
	
}

- (void)close
{
	
    [self p_animateClose];
	
}


#pragma mark - setters

- (void)setCountCoins:(NSUInteger)countCoins
{
	
	_countCoins = countCoins;
	
//	[self p_hideText];
	
    self.labelCoins.text = [NSString stringWithFormat: @"%lu", (unsigned long)countCoins];
	
//	[self p_showText];
	
}

#pragma mark - animation methids

- (void)p_animateOpen{
	
	[self p_hideText];
	
	[UIView animateWithDuration:0.001f
								   delay:0.0f
								 options:UIViewAnimationOptionCurveLinear
							  animations:^{
								  
								  [self.imagesFront[1] setAlpha:1];
								  [self.imagesFront[0] setAlpha:0];
								  
							  }
							  completion:nil];
	
	[UIView animateWithDuration:0.001f
								   delay:0.1f
								 options:UIViewAnimationOptionCurveLinear
							  animations:^{
								  
								  [self.imagesFront[2] setAlpha:1];
								  [self.imagesFront[1] setAlpha:0];
								  
							  }
							  completion:nil];
	
	[UIView animateWithDuration:0.001f
								   delay:0.2f
								 options:UIViewAnimationOptionCurveLinear
							  animations:^{
								  
								  [self.imagesFront[3] setAlpha:1];
								  [self.imagesFront[2] setAlpha:0];
								  
							  }
							  completion:^(BOOL finished) {
								  
								  [self p_koshelOpened];
								  
							  }];

}

- (void)p_animateClose
{
	
	[self p_showText];
	
	[UIView animateWithDuration:0.001f
								   delay:0.0f
								 options:UIViewAnimationOptionCurveLinear
							  animations:^{
								  
								  [self.imagesFront[2] setAlpha:1];
								  [self.imagesFront[3] setAlpha:0];
								  
							  }
							  completion:nil];
	
	[UIView animateWithDuration:0.001f
								   delay:0.1f
								 options:UIViewAnimationOptionCurveLinear
							  animations:^{
								  
								  [self.imagesFront[1] setAlpha:1];
								  [self.imagesFront[2] setAlpha:0];
								  
							  }
							  completion:nil];
	
	[UIView animateWithDuration:0.001f
						  delay:0.2f
						options:UIViewAnimationOptionCurveLinear
					 animations:^{
						 
						 [self.imagesFront[0] setAlpha:1];
						 [self.imagesFront[1] setAlpha:0];
						 
					 } completion:^(BOOL finished) {
						 
						 [self p_koshelClosed];
						 
					 }];

}

#pragma mark - done animation

- (void)p_koshelOpened{
	
	[[NSNotificationCenter defaultCenter] postNotificationName:ESM_KOSHEL_OPENNED_NOTIFICATION object:[self class]];
	
}

- (void)p_koshelClosed{
	
	[[NSNotificationCenter defaultCenter] postNotificationName:ESM_KOSHEL_CLOSED_NOTIFICATION object:[self class]];
	
}

#pragma mark - text update

- (void)p_hideText {
	
	[UIView animateWithDuration:ANIMATION_DURATION
						  delay:0
						options:UIViewAnimationOptionCurveLinear
					 animations:^{
						 
						 self.labelCoins.alpha = 0.0f;
						 
					 }
					 completion:nil];

}

- (void)p_showText{
	
	[UIView animateWithDuration:ANIMATION_DURATION
						  delay:0
						options:UIViewAnimationOptionCurveLinear
					 animations:^{
						 
						 self.labelCoins.alpha = 1.0f;
						 
					 }
					 completion:nil];

}

@end
