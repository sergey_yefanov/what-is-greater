//
//  ESMKoshelFrontView.h
//  Casino
//
//  Created by Ефанов Сергей on 04.08.14.
//
//

#import <UIKit/UIKit.h>

extern NSString * const ESM_KOSHEL_OPENNED_NOTIFICATION;
extern NSString * const ESM_KOSHEL_CLOSED_NOTIFICATION;

@interface ESMKoshelFrontView : UIView

@property (nonatomic) NSUInteger countCoins;

- (void)open;
- (void)close;

@end
