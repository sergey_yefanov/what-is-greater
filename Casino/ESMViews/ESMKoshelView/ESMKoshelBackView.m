//
//  ESMKoshelView.m
//  Casino
//
//  Created by Ефанов Сергей on 04.08.14.
//
//

#import "ESMKoshelBackView.h"
#import "ESMFileManager.h"

@interface ESMKoshelBackView()

@property (nonatomic) NSArray *		imagesBack;
@end

@implementation ESMKoshelBackView

#pragma mark - initialization

- (instancetype)init
{
	
	if (self = [super init]) {
		
		[self initialize];
		
	}
	
	return self;
	
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
	
	if (self = [super initWithCoder:aDecoder]) {
		
		[self initialize];
		
	}
	
	return self;
	
}

- (instancetype)initWithFrame:(CGRect)frame
{
	
	if (self = [super initWithFrame:frame]) {
		
		[self initialize];
		
	}
	
	return self;
	
}

- (void)initialize
{
	NSMutableArray *mImagesView = [NSMutableArray new];
	
	[self.imagesForAnimationBack enumerateObjectsUsingBlock:^(UIImage *image, NSUInteger idx, BOOL *stop) {
		
		UIImageView *imageView = [[UIImageView alloc] initWithFrame:self.bounds];
		imageView.image = image;
		imageView.contentMode = UIViewContentModeCenter;
		imageView.alpha = 0.0f;
		[self addSubview:imageView];
		[mImagesView addObject:imageView];
		
	}];
	
	self.imagesBack = mImagesView.copy;
	((UIImageView *)self.imagesBack[0]).alpha = 1.0f;
	
	self.backgroundColor = [UIColor clearColor];
	
}

- (NSArray *)imagesForAnimationBack
{
	
	static NSArray *arrAnimation;
	static dispatch_once_t onceToken;
	
	dispatch_once(&onceToken, ^{
		
		NSArray *namesImages = [[ESMFileManager sharedInstance] objectWithName:@"Images.plist"][@"Koshel"][@"Back"];
		arrAnimation = [NSArray new];
		
		for(NSString *imageName in namesImages) {

			arrAnimation = [arrAnimation arrayByAddingObject:[UIImage imageNamed:imageName]];
			
		}
		
	});
	
	return arrAnimation;
}

#pragma mark - public methods

-(void) open
{
	
    [self p_animateOpen];
	
}

- (void)close
{
	
    [self p_animateClose];
	
}

#pragma mark - animation methids

- (void)p_animateOpen{
	
	[UIView animateWithDuration:0.001f
								   delay:0.0f
								 options:UIViewAnimationOptionCurveLinear
							  animations:^{
								  
								  [self.imagesBack[1] setAlpha:1];
								  [self.imagesBack[0] setAlpha:0];
								  
							  }
							  completion:nil];
	
	[UIView animateWithDuration:0.001f
								   delay:0.1f
								 options:UIViewAnimationOptionCurveLinear
							  animations:^{
								  
								  [self.imagesBack[2] setAlpha:1];
								  [self.imagesBack[1] setAlpha:0];
								  
							  }
							  completion:nil];
	
	[UIView animateWithDuration:0.001f
								   delay:0.2f
								 options:UIViewAnimationOptionCurveLinear
							  animations:^{
								  
								  [self.imagesBack[3] setAlpha:1];
								  [self.imagesBack[2] setAlpha:0];
								  
							  }
							  completion:nil];

}

- (void)p_animateClose
{
	
	[UIView animateWithDuration:0.001f
								   delay:0.0f
								 options:UIViewAnimationOptionCurveLinear
							  animations:^{
								  
								  [self.imagesBack[2] setAlpha:1];
								  [self.imagesBack[3] setAlpha:0];
								  
							  }
							  completion:nil];
	
	[UIView animateWithDuration:0.001f
								   delay:0.1f
								 options:UIViewAnimationOptionCurveLinear
							  animations:^{
								  
								  [self.imagesBack[1] setAlpha:1];
								  [self.imagesBack[2] setAlpha:0];
								  
							  }
							  completion:nil];
	
	[UIView animateWithDuration:0.001f
						  delay:0.2f
						options:UIViewAnimationOptionCurveLinear
					 animations:^{
						 
						 [self.imagesBack[0] setAlpha:1];
						 [self.imagesBack[1] setAlpha:0];
						 
					 } completion:nil];

}

@end
