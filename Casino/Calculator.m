//
//  Calculator.m
//  Casino
//
//  Created by logo on 10/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Calculator.h"

@implementation Calculator

+(NSInteger) calculate:(NSString *)string{
    return [Calculator solve:[ProcesingStrings insertBracket:string]];
}

+(NSInteger) solve: (NSString*) string{
    
    if (!string) return 0;
    
    NSString* tempString = [string substringFromIndex:0];
    
    NSString* expressionA;
    NSString* expressionB;
    NSString* sign;
    int resultA;
    int resultB;
    
    NSInteger result;
    
    if([self lastSymbolIsCloseBrasket:tempString])  // *******)
    {
        expressionB = [ProcesingStrings cutTerm:tempString];
        
        if([self part:expressionB IsSimpleExpression:tempString]){ // simple expressions is a+b a-b a*b
            sign = [self cutSignFromDiferentExpression:tempString WithSimpleExpressionB:expressionB];
            expressionA = [self cutPartAFromDiferentExpression:tempString WithSimpleExpressionB:expressionB];
        }
        else {
            sign = nil;
            expressionA = nil;
        }
        
        resultA = [Calculator solve: expressionA];
        
        if(resultA < 0)
            return -1;
        
        resultB = [Calculator solve: expressionB];
        
        if(resultB < 0)
            return -1;
        
        if(!sign)
            return resultB;
        
        result = [Calculator solveWithIntA:resultA andStringSign:sign andIntB:resultB];
        return result >=0? result:-1;
    }
    
    if([self lastSymbolIsDigit:tempString]){
        expressionB = [ProcesingStrings cutDigit:tempString];
        
        if(expressionB.length < tempString.length){
            NSRange range;
            range.length = 1;
            range.location = tempString.length - (expressionB.length + 1);
            
            sign = [tempString substringWithRange:range];
            
            expressionA = [tempString substringToIndex:range.location];
        }
        else {
            sign = nil;
            expressionA = nil;
        }
        
        if(!sign){
            result = [expressionB intValue];
            return result>=0? result:-1;
        }
        
        result = [Calculator solveWithStringA:expressionA andStringSign:sign andStringB:expressionB];
        return result>=0? result:-1;
    }
    return -1;
}

+(BOOL) lastSymbolIsCloseBrasket:(NSString*) tempString{
    return [ProcesingStrings isCloseBracket:[tempString substringFromIndex:tempString.length - 1]];
}

+(BOOL) lastSymbolIsDigit:(NSString*) tempString{
    return [ProcesingStrings isDigit:[tempString substringFromIndex:tempString.length - 1]];
}

+(NSString*) cutSignFromDiferentExpression:(NSString*) diferentString WithSimpleExpressionB:(NSString*) simpleString{
    NSRange range;
    range.length = 1;
    range.location = diferentString.length - (simpleString.length + 3);
    
    return [diferentString substringWithRange:range];
}

+(NSString*) cutPartAFromDiferentExpression: (NSString*) diferentString WithSimpleExpressionB:(NSString*) simpleString{
    NSRange range;
    range.length = 1;
    range.location = diferentString.length - (simpleString.length + 3);
    
    return [diferentString substringToIndex:range.location];
}

+(NSInteger) solveWithStringA:(NSString*) A andStringSign:(NSString*) sign andStringB:(NSString*) B{
    if([ProcesingStrings isPlus:sign])
        return [Calculator solve:A] + [Calculator solve:B];
    
    if([ProcesingStrings isMultiply:sign])
        return [Calculator solve:A] * [Calculator solve:B];
    
    if([ProcesingStrings isMinus:sign])
        return [Calculator solve:A] - [Calculator solve:B];
    
    //else error
    return -1;
    
}

+(NSInteger) solveWithIntA:(NSInteger) A andStringSign:(NSString*) sign andIntB:(NSInteger) B{
    if([ProcesingStrings isPlus:sign])
        return A + B;
    
    if([ProcesingStrings isMultiply:sign])
        return A * B;
    
    if([ProcesingStrings isMinus:sign])
        return A - B;
    
    //else error
    return -1;
}

+(BOOL) part:(NSString*) part IsSimpleExpression:(NSString*) tempString{
    return part.length + 2 < tempString.length;
}

+(NSInteger) getRecordWithExpression:(Expression*) expression{
    //Получаем рекорд по переданной левой и правой части
    
    NSInteger result = 0;
    
    if(expression.leftPart.summ < expression.rightPart.summ)
    {
        for(int i = 0; i < 16; i++){
            for(int j = 0; j < 16; j++){
                
                if([expression leftLess]){
                    result = MAX(result, [expression getNumberClosedDigit]);
                }
                
            }
        }
        return result;
    }
    
    for(int i = 0; i < 16; i++){
        for(int j = 0; j < 16; j++){
            
            if([expression leftMore])
                result = MAX(result, [expression getNumberClosedDigit]);
        }
    }
    
    return result;
}

@end