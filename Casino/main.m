//
//  main.m
//  Casino
//
//  Created by Sergey Yefanov on 04.08.14.
//  Copyright (c) 2014 Sergey Yefanov. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ESMAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ESMAppDelegate class]));
    }
}