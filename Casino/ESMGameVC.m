//
//  Level.m
//  Casino
//
//  Created by Sergey Yefanov on 04.08.14.
//  Copyright (c) 2014 Sergey Yefanov. All rights reserved.
//

#import "ESMGameVC.h"
#import "ESMFileManager.h"
#import "ESMGameProcess.h"
#import "ESMGameView.h"

@interface ESMGameVC () <ESMGameViewDelegate>

@property (nonatomic)					ESMGameProcess *	gameProcess;
@property (nonatomic, weak) IBOutlet	ESMGameView *		gameView;
@property (nonatomic)					UIButton *			btnMenu;

@end

@implementation ESMGameVC

#pragma mark - load view

- (void)viewDidLoad
{
	
    [super viewDidLoad];
	
	self.gameView.delegate = self;
	self.gameProcess = [ESMGameProcess sharedInstance];
	
	// button menu
	self.btnMenu = [UIButton buttonWithType:UIButtonTypeCustom];
	self.btnMenu.frame = CGRectMake(10, 10, 150, 100);
	[self.btnMenu addTarget:self action:@selector(didTouchMenu) forControlEvents:UIControlEventTouchUpInside];
	[self.btnMenu setImage:[UIImage imageNamed:@"back_to_menu.png"] forState:UIControlStateNormal];
	[self.view addSubview:self.btnMenu];
	
	[[NSNotificationCenter defaultCenter] addObserverForName:ESM_GAME_EXPRESSON_LOADED_NOTIFICATION
													  object:[self.gameProcess class]
													   queue:nil
												  usingBlock:^(NSNotification *note) {
													  
													  self.gameView.data = note.userInfo;
													  
												  }];
	
	[[NSNotificationCenter defaultCenter] addObserverForName:ESM_NEW_COUNT_COINS_IN_KOSHEL_NOTIFICATION
													  object:[self.gameProcess class]
													   queue:nil
												  usingBlock:^(NSNotification *note) {
													  
													  self.gameView.numberCoinsInKoshel = ((NSNumber *)note.userInfo[ESM_NEW_COUNT_COINS_IN_KOSHEL_KEY]).integerValue;
													  
												  }];
	
	[[NSNotificationCenter defaultCenter] addObserverForName:ESM_GAME_PROCESS_PERCENT_CHANGED_NOTIFICATION
													  object:[self.gameProcess class]
													   queue:nil
												  usingBlock:^(NSNotification *note) {
													  
													  self.gameView.percent = ((NSNumber *)note.userInfo[ESM_GAME_PROCESS_KEY_PERCENT]).integerValue;
		 
												  }];
	
	[[NSNotificationCenter defaultCenter] addObserverForName:ESM_GAME_SIGN_PROCESSED_NOTIFICATION
													  object:[self.gameProcess class]
													   queue:nil
												  usingBlock:^(NSNotification *note) {
													  
													  NSDictionary *userInfo = note.userInfo;
													  ESMGameViewSign sign = ((NSNumber *)userInfo[ESM_GAME_SIGN_SELECTED_KEY]).integerValue;
													  BOOL isCorrect = ((NSNumber *)userInfo[ESM_GAME_SIGN_OK_KEY]).boolValue;
													  
													  [self.gameView sign:sign isCorrect:isCorrect];
													  [self.gameView moveAllKarts];
													  [self.gameView hideTactics];
														  
													  NSDictionary *stringsDict = userInfo[ESM_GAME_DICT_STRINGS_KEY];
														  
													  [self.gameView replaceExpressionsWithLeftPart:stringsDict[ESM_GAME_DICT_STRINGS_LEFT_KEY]
																					   andRightPart:stringsDict[ESM_GAME_DICT_STRINGS_RIGHT_KEY]];
													  
													  [self.gameView showButtonNext];
													  
													  if (isCorrect) {
														  
														  [self.gameView moveAllCoinsToKoshel];
														  
														  if(self.gameMode == ESMGameModeGame) {
														  
															  [self.gameView showWinStatusWithCoins:((NSNumber *)userInfo[ESM_GAME_DICT_COINS_WIN_KEY]).integerValue];
																  
														  } else {
															  
															  [self.gameView showWinStatus];
															  
														  }
														  
													  } else {
														  
														  [self.gameView hideAllCoins];
														  
														  
														  if (((NSNumber *)userInfo[@"ESM_GAME_SELECTED_SIGN_KEY"]).integerValue == ESMGameSelectedSignLess) {
														  
															  [self.gameView showFailStatusWithMorePart:@"LeftPart"];
															  
														  } else {
															  
															  [self.gameView showFailStatusWithMorePart:@"RightPart"];
															  
														  }
														  
													  }
													  
												  }];
	
	if (self.gameMode == ESMGameModeGame) {
	
		
		self.gameProcess.gameState = ESMGameStateGame;
		self.gameView.gameState = ESMGameViewStateGame;
		self.gameProcess.levelIndex = self.levelIndex;
		[self setBackgroundAtIndex:self.gameProcess.levelIndex];
		[self.gameProcess startGame];
		
	} else if (self.gameMode == ESMGameModeTrainer) {
		
		
		self.gameProcess.gameState = ESMGameStateTrainer;
		self.gameView.gameState = ESMGameViewStateGameTrainer;
		[self setBackgroundAtIndex:0];
		[self.gameProcess startTrainer];
		
	}

}

#pragma mark - Getters

- (NSArray *)backgroundsArr
{
	
	static NSArray *imagesArr;
	static dispatch_once_t onceToken;
	
	dispatch_once(&onceToken, ^{
		
		NSArray *arrPaths = [[[ESMFileManager sharedInstance] objectWithName:@"Images.plist"] objectForKey:@"Backgrounds"];
		imagesArr = [NSArray new];
		
		for(NSString *imagePath in arrPaths) {
			
			imagesArr = [imagesArr arrayByAddingObject:[UIImage imageNamed:imagePath]];
			
		}
		
	});
	
	return imagesArr;
	
}

#pragma mark - interface methods

- (void)setBackgroundAtIndex:(NSInteger)index
{
	
	self.gameView.backgroundColor = [UIColor colorWithPatternImage:self.backgroundsArr[index]];
	
}

- (void)animateNextExpression
{
	
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0f];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp
						   forView:self.view
							 cache:NO];
    [UIView commitAnimations];
	
}

#pragma mark - IBAction methods

- (void)didTouchMenu
{
    
	[self.gameProcess stopGame];
    [Flurry endTimedEvent:@"LEVEL_RUNNED" withParameters:nil];
    [Flurry endTimedEvent:@"LOG_TRAINER_TOUCH" withParameters:nil];
    [self.navigationController popViewControllerAnimated:YES];
	
}

#pragma mark - game view delegate

- (void)gameViewDidTouchNextExpression:(ESMGameView *)gameView
{
	
	[self animateNextExpression];
	
	BOOL isLoaded;
	
	
	if (self.gameMode == ESMGameModeGame) {
		
		isLoaded = [self.gameProcess loadRandomExpressionInLevel:self.levelIndex];
		
	} else {
		
		isLoaded = [self.gameProcess loadRandomExpression];
		
	}
	
	if (!isLoaded) {
		
		[self didTouchMenu];
		
	}
	
}

- (void)gameView:(ESMGameView *)gameView didTouchSign:(ESMGameViewSign)sign
{
	
	ESMGameSelectedSign gameSelectedSign;
	
	if (sign == ESMGameViewSignLess) {
		
		gameSelectedSign = ESMGameSelectedSignLess;
		[self.gameView hideMore];
		
	} else {
		
		gameSelectedSign = ESMGameSelectedSignMore;
		[self.gameView hideLess];
		
	}
	
	[self.gameProcess didSelectSign:gameSelectedSign];
	
}

- (void)gameView:(ESMGameView *)gameView didTouchSymbolAtIndex:(NSUInteger)index
{
	
	if ([self.gameProcess openSymbolAtIndex:index]) {
		
		[self.gameView hideCoin];
		[self.gameView openSymbolAtIndex:index];
		
	}
	
}

- (void)gameViewDidTouchWin:(ESMGameView *)gameView
{
	
	[gameView blinkSymbolsAtIndexes:self.gameProcess.recordCombinationIndexes];
	
}

- (void)dealloc
{
	
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	
}

@end
