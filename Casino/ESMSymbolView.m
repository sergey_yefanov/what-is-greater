//
//  ESMSymbolView.m
//  Casino
//
//  Created by Sergey Yefanov on 04.08.14.
//  Copyright (c) 2014 Sergey Yefanov. All rights reserved.
//

#import "ESMSymbolView.h"
#import "ESMFileManager.h"
#import "NSString + Expression.h"

static CGFloat const ANIMATION_DURATION = 1.0f;

typedef UIImageView ESMKart;
typedef UIImageView ESMSymbol;

@interface ESMSymbolView ()

@property (nonatomic) ESMKart *		kartView;
@property (nonatomic) ESMSymbol *	symbolView;

@end

@implementation ESMSymbolView

#pragma mark - initialization

- (instancetype)init
{
	
	if (self = [super init]) {
		
		[self initialize];
		
	}
	
	return self;
	
}

- (instancetype)initWithFrame:(CGRect)frame
{
	
	if (self = [super initWithFrame:frame]) {
		
		[self initialize];
		
	}
	
	return self;
	
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
	
	if (self = [super initWithCoder:aDecoder]) {
		
		[self initialize];
		
	}
	
	return self;
	
}

- (void)initialize
{
	
	self.symbolView = [[ESMSymbol alloc] initWithFrame:self.bounds];
	self.symbolView.contentMode = UIViewContentModeCenter;
	[self addSubview:self.symbolView];
	
	self.kartView = [[ESMKart alloc] initWithFrame:self.bounds];
	self.kartView.contentMode = UIViewContentModeCenter;
	self.kartView.animationImages = self.animationArr;
	self.kartView.animationDuration = ANIMATION_DURATION;
	self.kartView.animationRepeatCount = 1;
	self.kartView.image = self.animationArr[0];
	
	[self addSubview:self.kartView];
	
	self.kartView.userInteractionEnabled = YES;
	
}

#pragma mark - Setters

- (void)setOpen:(BOOL)open
{
	
	_open = open;
	self.symbolView.hidden = !open;
	self.kartView.hidden = open;
	
}

- (void)setSymbol:(NSString *)symbol
{
	
	_symbol = symbol;
	self.symbolView.image = self.imagesDict[symbol];
	
}

#pragma mark - Getters

- (NSDictionary *)imagesDict
{
	
	static NSDictionary *dictDigit = nil;
	static dispatch_once_t onceToken;
	
	dispatch_once(&onceToken, ^{
		
		NSMutableDictionary *imagesDict = [NSMutableDictionary new];
		NSDictionary *pathsDict = [[[ESMFileManager sharedInstance] objectWithName:@"Images.plist"] objectForKey:@"Didgits"];
		
		[pathsDict enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSString *obj, BOOL *stop) {
			
			[imagesDict setObject:[UIImage imageNamed:obj] forKey:key];
			
		}];
		
		dictDigit = imagesDict.copy;
		
	});
	
	return dictDigit;
	
}

- (NSArray *)animationArr
{
	
	static NSArray *arrAnimation;
	static dispatch_once_t onceToken;
	
	dispatch_once(&onceToken, ^{
	
		NSArray *animationPaths = [[ESMFileManager sharedInstance] objectWithName:@"Images.plist"][@"Karta"];
		arrAnimation = [NSArray new];
		
		for (NSString *path in animationPaths) {
			
			arrAnimation = [arrAnimation arrayByAddingObject:[UIImage imageNamed:path]];
			
		}
		
	});
	
	return arrAnimation;
	
}

#pragma mark -

- (void)blink
{
	
	if (!self.isOpen) {
		
		return;
		
	}
	
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0f];
    [UIView setAnimationRepeatCount:100.0f];
    [UIView setAnimationCurve:UIViewAnimationCurveLinear];
    [UIView setAnimationDelegate:self];
	
    self.alpha = 0.0;
	
    [UIView commitAnimations];
	
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0f];
    [UIView setAnimationRepeatCount:100.0f];
    [UIView setAnimationCurve:UIViewAnimationCurveLinear];
    [UIView setAnimationDelegate:self];
	
    self.alpha = 1.0;
	
    [UIView commitAnimations];
	
}

- (void)open
{
	
    self.symbolView.hidden = NO;
	
    [self.kartView startAnimating];
    [UIView animateWithDuration:0.0f
						  delay: ANIMATION_DURATION - 0.01
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
						 
                         self.kartView.alpha = 0.0;
						 
                     } completion:^(BOOL finished) {
						 
						self.open = YES;
						 
					 }];
	
}

- (void)move
{
	
	self.symbolView.hidden = NO;
	
	[UIView animateWithDuration:1.0f
						  delay:0.0f
						options:UIViewAnimationOptionCurveLinear
					 animations:^{
						 
						self.kartView.center = CGPointMake(50, -30);
						 
					 } completion:nil];

	
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	
	if (self.symbol.isDigit) {
	
		if ([self.delegate respondsToSelector:@selector(symbolDidTouch:)]) {
			
			[self.delegate symbolDidTouch:self];
			
		} else {
			
			NSLog(@"delegate have not selector symbolDidTouch:");
			
		}
		
	}
	
}

//- (uint32_t) getPixel:(CGPoint) point;
//{
//	GLuint components;
//	GLuint imgWide, imgHigh;      // Real image size
//	GLuint rowBytes, rowPixels;   // Image size padded by CGImage
//	CGBitmapInfo info;            // CGImage component layout info
//	CGColorSpaceModel colormodel; // CGImage colormodel (RGB, CMYK, paletted, etc)
//	GLenum internal, format;
//	GLubyte *pixels = NULL;
//	int x1=lrintf(point.x);
//	int y1=lrintf(point.y);
//	//printf("Point = %d : %d \n", x, y);
//	if (! CGRectContainsPoint([self bounds], point))
//	{
//		return 0;
//	}
//	CGImageRef CGImage = self.kartView.image.CGImage;
//    //	rt_assert(CGImage);
//	if (!CGImage)
//		return 0;
//	
//	// Parse CGImage info
//	info       = CGImageGetBitmapInfo(CGImage);		// CGImage may return pixels in RGBA, BGRA, or ARGB order
//	colormodel = CGColorSpaceGetModel(CGImageGetColorSpace(CGImage));
//	size_t bpp = CGImageGetBitsPerPixel(CGImage);
//	if (bpp < 8 || bpp > 32 || (colormodel != kCGColorSpaceModelMonochrome && colormodel != kCGColorSpaceModelRGB))
//	{
//		// This loader does not support all possible CGImage types, such as paletted images
//        //		CGImageRelease(CGImage);
//		return 0;
//	}
//    
//	components = bpp>>3;
//	rowBytes   = CGImageGetBytesPerRow(CGImage);	// CGImage may pad rows
//	rowPixels  = rowBytes / components;
//	imgWide    = CGImageGetWidth(CGImage);
//	imgHigh    = CGImageGetHeight(CGImage);
//	//printf("Size= %d : %d \n", imgWide, imgHigh);
//	
//	// Choose OpenGL format
//	switch(bpp)
//	{
//		default:
//            
//		case 32:
//		{
//			internal = GL_RGBA;
//			switch(info & kCGBitmapAlphaInfoMask)
//			{
//				case kCGImageAlphaPremultipliedFirst:
//				case kCGImageAlphaFirst:
//				case kCGImageAlphaNoneSkipFirst:
//					format = GL_BGRA;
//					break;
//				default:
//					format = GL_RGBA;
//			}
//			break;
//		}
//		case 24:
//			internal = format = GL_RGB;
//			break;
//		case 16:
//			internal = format = GL_LUMINANCE_ALPHA;
//			break;
//		case 8:
//			internal = format = GL_LUMINANCE;
//			break;
//	}
//	
//	// Get a pointer to the uncompressed image data.
//	//
//	// This allows access to the original (possibly unpremultiplied) data, but any manipulation
//	// (such as scaling) has to be done manually. Contrast this with drawing the image
//	// into a CGBitmapContext, which allows scaling, but always forces premultiplication.
//	CFDataRef data = CGDataProviderCopyData(CGImageGetDataProvider(CGImage));
//	pixels = (GLubyte *)CFDataGetBytePtr(data);
//	uint32_t pixel = ((uint32_t *)pixels)[y1 * rowPixels + x1];
//	
//	// If the CGImage component layout isn't compatible with OpenGL, fix it.
//	// On the device, CGImage will generally return BGRA or RGBA.
//	// On the simulator, CGImage may return ARGB, depending on the file format.
//	if (format == GL_BGRA)
//	{
//		if ((info & kCGBitmapByteOrderMask) != kCGBitmapByteOrder32Host)
//		{
//			// Convert from ARGB to BGRA
//            pixel = (pixel << 24) | ((pixel & 0xFF00) << 8) | ((pixel >> 8) & 0xFF00) | (pixel >> 24);
//		}
//		
//		// All current iPhoneOS devices support BGRA via an extension.
//	}
//	//printf("Pixel = %x \n", pixel);
//	CFRelease(data);
//	return pixel;
//}
//
//- (BOOL) pointInside:(CGPoint)point withEvent:(UIEvent *)event {
//	return 	0 != (0xFF000000 & [self getPixel:point]);
//}

@end
